<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e1b05806-6d39-4d28-a80f-737dffbf7294(org.clawiz.bpm.common.language.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="brs9" ref="r:75f31872-7db8-4aa8-8319-936e3ef4dbdd(org.clawiz.bpm.common.language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="6P7bv1gqyJu">
    <property role="3GE5qa" value="process.node.event.end" />
    <ref role="1M2myG" to="brs9:6P7bv1gqww0" resolve="NoneEndEvent" />
  </node>
  <node concept="1M2fIO" id="6P7bv1gr41G">
    <property role="3GE5qa" value="process.flow" />
    <ref role="1M2myG" to="brs9:zNXZXsLjw$" resolve="AbstractFlow" />
    <node concept="1N5Pfh" id="6P7bv1gr41H" role="1Mr941">
      <ref role="1N5Vy1" to="brs9:6P7bv1gqTdF" resolve="targetNode" />
      <node concept="1dDu$B" id="6P7bv1gr41J" role="1N6uqs">
        <ref role="1dDu$A" to="brs9:6P7bv1gqwsx" resolve="AbstractProcessNode" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="6P7bv1guRks">
    <property role="3GE5qa" value="process.node" />
    <ref role="1M2myG" to="brs9:6P7bv1gqwsx" resolve="AbstractProcessNode" />
    <node concept="1N5Pfh" id="6P7bv1guRkt" role="1Mr941">
      <ref role="1N5Vy1" to="brs9:6P7bv1guR7s" resolve="lane" />
      <node concept="1dDu$B" id="6P7bv1guRkv" role="1N6uqs">
        <ref role="1dDu$A" to="brs9:6P7bv1guR3a" resolve="Lane" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="UIK3MHoegt">
    <property role="3GE5qa" value="process.node.activity.task.service" />
    <ref role="1M2myG" to="brs9:UIK3MHoed8" resolve="SetFieldValueServiceTaskActivity" />
    <node concept="1N5Pfh" id="UIK3MHoegu" role="1Mr941">
      <ref role="1N5Vy1" to="brs9:UIK3MHoedc" resolve="typeField" />
      <node concept="1dDu$B" id="UIK3MHoegx" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmPQb" resolve="TypeField" />
      </node>
    </node>
  </node>
</model>

