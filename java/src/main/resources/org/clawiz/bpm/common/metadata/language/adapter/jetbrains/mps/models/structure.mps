<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:75f31872-7db8-4aa8-8319-936e3ef4dbdd(org.clawiz.bpm.common.language.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="2303sVWgwbc">
    <property role="EcuMT" value="2359901387388748492" />
    <property role="TrG5h" value="Process" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="process" />
    <property role="34LRSv" value="Process" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="2KIKjjWf5oM" role="1TKVEl">
      <property role="IQ2nx" value="3183694419911792178" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6P7bv1gs$Dl" role="1TKVEl">
      <property role="IQ2nx" value="7874312983645669973" />
      <property role="TrG5h" value="startOnNew" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="PrWs8" id="2303sVWgwbk" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="2KIKjjWf5oG" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="2KIKjjWft1A" role="1TKVEi">
      <property role="IQ2ns" value="3183694419911888998" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2KIKjjWft1m" resolve="AbstractProcessProperty" />
    </node>
    <node concept="1TJgyj" id="6P7bv1gqwsG" role="1TKVEi">
      <property role="IQ2ns" value="7874312983645128492" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="nodes" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6P7bv1gqwsx" resolve="AbstractProcessNode" />
    </node>
    <node concept="1TJgyj" id="6P7bv1guR3$" role="1TKVEi">
      <property role="IQ2ns" value="7874312983646269668" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="lanes" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6P7bv1guR3a" resolve="Lane" />
    </node>
    <node concept="1TJgyj" id="zNXZXsL1CP" role="1TKVEi">
      <property role="IQ2ns" value="645131847789189685" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="rootType" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmg18" resolve="Type" />
    </node>
    <node concept="1TJgyj" id="6P7bv1guV5p" role="1TKVEi">
      <property role="IQ2ns" value="7874312983646286169" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="pool" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="6P7bv1guV4V" resolve="Pool" />
    </node>
  </node>
  <node concept="1TIwiD" id="2KIKjjWft1m">
    <property role="EcuMT" value="3183694419911888982" />
    <property role="3GE5qa" value="process.property" />
    <property role="TrG5h" value="AbstractProcessProperty" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="zNXZXsL1Dk">
    <property role="EcuMT" value="645131847789189716" />
    <property role="3GE5qa" value="process.node.event" />
    <property role="TrG5h" value="AbstractEvent" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="6P7bv1gqwsx" resolve="AbstractProcessNode" />
    <node concept="PrWs8" id="zNXZXsL1Do" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="zNXZXsLjw$">
    <property role="EcuMT" value="645131847789262884" />
    <property role="3GE5qa" value="process.flow" />
    <property role="TrG5h" value="AbstractFlow" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="6flZoDGqlWI" role="1TKVEl">
      <property role="IQ2nx" value="7193934750760394542" />
      <property role="TrG5h" value="caption" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="1jWl1Kgi3cf" role="1TKVEi">
      <property role="IQ2ns" value="1512176054398628623" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="conditions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2EwC_z7Ff$z" resolve="AbstractFlowCondition" />
    </node>
    <node concept="1TJgyj" id="6P7bv1gqTdF" role="1TKVEi">
      <property role="IQ2ns" value="7874312983645229931" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="targetNode" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6P7bv1gqwsx" resolve="AbstractProcessNode" />
    </node>
    <node concept="PrWs8" id="zNXZXsLjw_" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="zNXZXsLjwB">
    <property role="EcuMT" value="645131847789262887" />
    <property role="3GE5qa" value="process.node.gateway" />
    <property role="TrG5h" value="AbstractGateway" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="6P7bv1gqwsx" resolve="AbstractProcessNode" />
    <node concept="PrWs8" id="zNXZXsLjwC" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="zNXZXsLjwK">
    <property role="EcuMT" value="645131847789262896" />
    <property role="3GE5qa" value="process.node.activity" />
    <property role="TrG5h" value="AbstractActivity" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="6P7bv1gqwsx" resolve="AbstractProcessNode" />
    <node concept="PrWs8" id="zNXZXsLjwL" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="zNXZXsLjwP">
    <property role="EcuMT" value="645131847789262901" />
    <property role="3GE5qa" value="process.node.event.start" />
    <property role="TrG5h" value="AbstractStartEvent" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="zNXZXsL1Dk" resolve="AbstractEvent" />
  </node>
  <node concept="1TIwiD" id="zNXZXsLjwQ">
    <property role="EcuMT" value="645131847789262902" />
    <property role="3GE5qa" value="process.node.event.end" />
    <property role="TrG5h" value="AbstractEndEvent" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="zNXZXsL1Dk" resolve="AbstractEvent" />
    <node concept="PrWs8" id="zNXZXsLjwR" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="zNXZXsLjwZ">
    <property role="EcuMT" value="645131847789262911" />
    <property role="3GE5qa" value="process.node.activity.task" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractTaskActivity" />
    <ref role="1TJDcQ" node="zNXZXsLjwK" resolve="AbstractActivity" />
  </node>
  <node concept="1TIwiD" id="zNXZXsLjx8">
    <property role="EcuMT" value="645131847789262920" />
    <property role="3GE5qa" value="process.node.activity.task.service" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractServiceTaskActivity" />
    <ref role="1TJDcQ" node="zNXZXsLjwZ" resolve="AbstractTaskActivity" />
  </node>
  <node concept="1TIwiD" id="6P7bv1gqwsx">
    <property role="EcuMT" value="7874312983645128481" />
    <property role="3GE5qa" value="process.node" />
    <property role="TrG5h" value="AbstractProcessNode" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6P7bv1guR7s" role="1TKVEi">
      <property role="IQ2ns" value="7874312983646269916" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="lane" />
      <ref role="20lvS9" node="6P7bv1guR3a" resolve="Lane" />
    </node>
    <node concept="PrWs8" id="6P7bv1gqwsy" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="6P7bv1gqwvY" role="1TKVEi">
      <property role="IQ2ns" value="7874312983645128702" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="outgoingFlows" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="zNXZXsLjw$" resolve="AbstractFlow" />
    </node>
    <node concept="1TJgyi" id="6P7bv1guYFG" role="1TKVEl">
      <property role="IQ2nx" value="7874312983646300908" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="6P7bv1gqww0">
    <property role="EcuMT" value="7874312983645128704" />
    <property role="3GE5qa" value="process.node.event.end" />
    <property role="TrG5h" value="NoneEndEvent" />
    <property role="34LRSv" value="end" />
    <ref role="1TJDcQ" node="zNXZXsLjwQ" resolve="AbstractEndEvent" />
  </node>
  <node concept="1TIwiD" id="6P7bv1gqww3">
    <property role="EcuMT" value="7874312983645128707" />
    <property role="3GE5qa" value="process.node.event.start" />
    <property role="TrG5h" value="NoneStartEvent" />
    <property role="34LRSv" value="start" />
    <ref role="1TJDcQ" node="zNXZXsLjwP" resolve="AbstractStartEvent" />
  </node>
  <node concept="1TIwiD" id="6P7bv1gqQYS">
    <property role="EcuMT" value="7874312983645220792" />
    <property role="3GE5qa" value="process.node.activity.task.user" />
    <property role="TrG5h" value="AbstractUserTaskActivity" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="zNXZXsLjwZ" resolve="AbstractTaskActivity" />
  </node>
  <node concept="1TIwiD" id="6P7bv1gqQYV">
    <property role="EcuMT" value="7874312983645220795" />
    <property role="3GE5qa" value="process.node.activity.task.user" />
    <property role="TrG5h" value="UserTaskActivity" />
    <property role="34LRSv" value="user task" />
    <ref role="1TJDcQ" node="6P7bv1gqQYS" resolve="AbstractUserTaskActivity" />
  </node>
  <node concept="1TIwiD" id="6P7bv1gqTdH">
    <property role="EcuMT" value="7874312983645229933" />
    <property role="3GE5qa" value="process.flow.sequence" />
    <property role="TrG5h" value="SequenceFlow" />
    <property role="34LRSv" value="-&gt;" />
    <ref role="1TJDcQ" node="6P7bv1guo8Y" resolve="AbstractSequenceFlow" />
  </node>
  <node concept="1TIwiD" id="6P7bv1grfW0">
    <property role="EcuMT" value="7874312983645323008" />
    <property role="3GE5qa" value="process.node.activity.task.service" />
    <property role="TrG5h" value="ExecuteServiceMethodServiceTaskActivity" />
    <property role="34LRSv" value="execute service method" />
    <ref role="1TJDcQ" node="zNXZXsLjx8" resolve="AbstractServiceTaskActivity" />
    <node concept="1TJgyj" id="6P7bv1grfW1" role="1TKVEi">
      <property role="IQ2ns" value="7874312983645323009" />
      <property role="20lmBu" value="aggregation" />
      <property role="20lbJX" value="1" />
      <property role="20kJfa" value="serviceMethod" />
      <ref role="20lvS9" to="lehn:1WjxkW0cEWa" resolve="ServiceMethodRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="6P7bv1guo8Y">
    <property role="EcuMT" value="7874312983646143038" />
    <property role="3GE5qa" value="process.flow.sequence" />
    <property role="TrG5h" value="AbstractSequenceFlow" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="zNXZXsLjw$" resolve="AbstractFlow" />
  </node>
  <node concept="1TIwiD" id="6P7bv1guR3a">
    <property role="EcuMT" value="7874312983646269642" />
    <property role="3GE5qa" value="process.swimlane.lane" />
    <property role="TrG5h" value="Lane" />
    <ref role="1TJDcQ" node="6aK4n5uBJ$L" resolve="AbstractLane" />
  </node>
  <node concept="1TIwiD" id="6P7bv1guV4V">
    <property role="EcuMT" value="7874312983646286139" />
    <property role="3GE5qa" value="process.swimlane.pool" />
    <property role="TrG5h" value="Pool" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Pool" />
    <ref role="1TJDcQ" node="6aK4n5uBJ$Q" resolve="AbstractPool" />
  </node>
  <node concept="1TIwiD" id="2EwC_z7Ff$z">
    <property role="EcuMT" value="3071633448057501987" />
    <property role="3GE5qa" value="process.flow.condition" />
    <property role="TrG5h" value="AbstractFlowCondition" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="1jWl1Kgh59p">
    <property role="EcuMT" value="1512176054398374489" />
    <property role="3GE5qa" value="process.node.activity.task.message.send" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractSendTaskActivity" />
    <ref role="1TJDcQ" node="1jWl1Kghv$G" resolve="AbstractMessageTaskActivity" />
    <node concept="1TJgyj" id="1jWl1Kgh5bD" role="1TKVEi">
      <property role="IQ2ns" value="1512176054398374633" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="targetProcess" />
      <ref role="20lvS9" node="2303sVWgwbc" resolve="Process" />
    </node>
  </node>
  <node concept="1TIwiD" id="1jWl1Kgh59q">
    <property role="EcuMT" value="1512176054398374490" />
    <property role="3GE5qa" value="process.node.activity.task.message.send" />
    <property role="TrG5h" value="SendTaskActivity" />
    <property role="34LRSv" value="send" />
    <ref role="1TJDcQ" node="1jWl1Kgh59p" resolve="AbstractSendTaskActivity" />
  </node>
  <node concept="1TIwiD" id="1jWl1Kgh59s">
    <property role="EcuMT" value="1512176054398374492" />
    <property role="3GE5qa" value="process.node.activity.task.message" />
    <property role="TrG5h" value="AbstractMessage" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1jWl1Kgh59y" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1jWl1Kgh59t">
    <property role="EcuMT" value="1512176054398374493" />
    <property role="3GE5qa" value="process.node.activity.task.message" />
    <property role="TrG5h" value="StringMessage" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="String message" />
    <ref role="1TJDcQ" node="1jWl1Kgh59s" resolve="AbstractMessage" />
  </node>
  <node concept="1TIwiD" id="1jWl1Kgh5hx">
    <property role="EcuMT" value="1512176054398375009" />
    <property role="3GE5qa" value="process.node.activity.task.message.receive" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractReceiveTaskActivity" />
    <ref role="1TJDcQ" node="1jWl1Kghv$G" resolve="AbstractMessageTaskActivity" />
  </node>
  <node concept="1TIwiD" id="1jWl1Kgh5h$">
    <property role="EcuMT" value="1512176054398375012" />
    <property role="3GE5qa" value="process.node.activity.task.message.receive" />
    <property role="TrG5h" value="ReceiveTaskActivity" />
    <property role="34LRSv" value="receive" />
    <ref role="1TJDcQ" node="1jWl1Kgh5hx" resolve="AbstractReceiveTaskActivity" />
  </node>
  <node concept="1TIwiD" id="1jWl1Kghv$G">
    <property role="EcuMT" value="1512176054398482732" />
    <property role="3GE5qa" value="process.node.activity.task.message" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractMessageTaskActivity" />
    <ref role="1TJDcQ" node="zNXZXsLjwZ" resolve="AbstractTaskActivity" />
    <node concept="1TJgyj" id="1jWl1Kghv$J" role="1TKVEi">
      <property role="IQ2ns" value="1512176054398482735" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="message" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="1jWl1Kgh59s" resolve="AbstractMessage" />
    </node>
  </node>
  <node concept="1TIwiD" id="1jWl1KghXTf">
    <property role="EcuMT" value="1512176054398606927" />
    <property role="3GE5qa" value="process.flow.condition.eval" />
    <property role="TrG5h" value="JavaScriptEvalFlowCondition" />
    <property role="34LRSv" value="javascript eval" />
    <ref role="1TJDcQ" node="1jWl1KghXTg" resolve="AbstractEvalFlowCondition" />
  </node>
  <node concept="1TIwiD" id="1jWl1KghXTg">
    <property role="EcuMT" value="1512176054398606928" />
    <property role="3GE5qa" value="process.flow.condition.eval" />
    <property role="TrG5h" value="AbstractEvalFlowCondition" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="2EwC_z7Ff$z" resolve="AbstractFlowCondition" />
    <node concept="1TJgyi" id="1jWl1KghXTh" role="1TKVEl">
      <property role="IQ2nx" value="1512176054398606929" />
      <property role="TrG5h" value="expression" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="1jWl1KgihZk">
    <property role="EcuMT" value="1512176054398689236" />
    <property role="3GE5qa" value="process.flow.condition.message" />
    <property role="TrG5h" value="ReceiveMessageFlowCondition" />
    <property role="34LRSv" value="receive message" />
    <ref role="1TJDcQ" node="2EwC_z7Ff$z" resolve="AbstractFlowCondition" />
    <node concept="1TJgyj" id="1jWl1KgihZl" role="1TKVEi">
      <property role="IQ2ns" value="1512176054398689237" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="message" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1jWl1Kgh59s" resolve="AbstractMessage" />
    </node>
  </node>
  <node concept="1TIwiD" id="52sT7wuT9ob">
    <property role="EcuMT" value="5808768823966602763" />
    <property role="3GE5qa" value="process.flow.condition.service" />
    <property role="TrG5h" value="ServiceMethodFlowCondition" />
    <property role="34LRSv" value="service method" />
    <ref role="1TJDcQ" node="2EwC_z7Ff$z" resolve="AbstractFlowCondition" />
    <node concept="1TJgyj" id="52sT7wuT9oc" role="1TKVEi">
      <property role="IQ2ns" value="5808768823966602764" />
      <property role="20lmBu" value="aggregation" />
      <property role="20lbJX" value="1" />
      <property role="20kJfa" value="serviceMethod" />
      <ref role="20lvS9" to="lehn:1WjxkW0cEWa" resolve="ServiceMethodRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="UIK3MHoed8">
    <property role="EcuMT" value="1057994329318024008" />
    <property role="3GE5qa" value="process.node.activity.task.service" />
    <property role="TrG5h" value="SetFieldValueServiceTaskActivity" />
    <property role="34LRSv" value="set field value" />
    <ref role="1TJDcQ" node="zNXZXsLjx8" resolve="AbstractServiceTaskActivity" />
    <node concept="1TJgyi" id="UIK3MHoedf" role="1TKVEl">
      <property role="IQ2nx" value="1057994329318024015" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="UIK3MHoedc" role="1TKVEi">
      <property role="IQ2ns" value="1057994329318024012" />
      <property role="20lmBu" value="reference" />
      <property role="20lbJX" value="1" />
      <property role="20kJfa" value="typeField" />
      <ref role="20lvS9" to="lehn:6porqNrmPQb" resolve="TypeField" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uBJ$L">
    <property role="EcuMT" value="7111202990234466609" />
    <property role="3GE5qa" value="process.swimlane.lane" />
    <property role="TrG5h" value="AbstractLane" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6aK4n5uBJ$M" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6aK4n5uBJ$Q">
    <property role="EcuMT" value="7111202990234466614" />
    <property role="3GE5qa" value="process.swimlane.pool" />
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractPool" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6aK4n5uBJ$T" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
</model>

