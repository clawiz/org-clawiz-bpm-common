<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1f22743e-fc26-4ea7-9264-b35847912de9(org.clawiz.bpm.common.language.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="brs9" ref="r:75f31872-7db8-4aa8-8319-936e3ef4dbdd(org.clawiz.bpm.common.language.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="2303sVWgxeI">
    <property role="3GE5qa" value="process" />
    <ref role="1XX52x" to="brs9:2303sVWgwbc" resolve="Process" />
    <node concept="3EZMnI" id="2303sVWgxeK" role="2wV5jI">
      <node concept="3F0ifn" id="2303sVWgxeR" role="3EZMnx">
        <property role="3F0ifm" value="Process" />
      </node>
      <node concept="3F0A7n" id="2303sVWgxeX" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="2KIKjjWf5q7" role="3EZMnx">
        <node concept="pVoyu" id="2KIKjjWf7L9" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="1SGrquhqaz8" role="3EZMnx">
        <property role="3F0ifm" value="object type  " />
        <node concept="pVoyu" id="1SGrquhqaz9" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="1SGrquhqazW" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="6P7bv1gqNzW" role="3EZMnx">
        <ref role="1NtTu8" to="brs9:zNXZXsL1CP" resolve="rootType" />
        <node concept="1sVBvm" id="6P7bv1gqNzY" role="1sWHZn">
          <node concept="3F0A7n" id="6P7bv1gqN$F" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1gt5fa" role="3EZMnx">
        <property role="3F0ifm" value="start on new " />
        <node concept="pVoyu" id="6P7bv1gt5fb" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1gt5fc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6P7bv1gt5gB" role="3EZMnx">
        <ref role="1NtTu8" to="brs9:6P7bv1gs$Dl" resolve="startOnNew" />
      </node>
      <node concept="3F0ifn" id="6P7bv1gqNyL" role="3EZMnx">
        <property role="3F0ifm" value="description  " />
        <node concept="pVoyu" id="6P7bv1gqNyM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1gqNyN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="2KIKjjWf5r4" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="brs9:2KIKjjWf5oM" resolve="description" />
      </node>
      <node concept="3F0ifn" id="6P7bv1guVaR" role="3EZMnx">
        <node concept="pVoyu" id="6P7bv1guVaS" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1guVaT" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1gqwta" role="3EZMnx">
        <property role="3F0ifm" value="pool         " />
        <node concept="pVoyu" id="6P7bv1gqwtb" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1gqwtc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="6P7bv1guV9U" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="brs9:6P7bv1guV5p" resolve="pool" />
        <node concept="1sVBvm" id="6P7bv1guV9W" role="1sWHZn">
          <node concept="3F0A7n" id="6P7bv1guVaO" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1guVbL" role="3EZMnx">
        <node concept="pVoyu" id="6P7bv1guVbM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1guVbN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2KIKjjWft1F" role="3EZMnx">
        <property role="3F0ifm" value="properties   " />
        <node concept="pVoyu" id="2KIKjjWft1G" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="2KIKjjWft1H" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="2KIKjjWft3I" role="3EZMnx">
        <ref role="1NtTu8" to="brs9:2KIKjjWft1A" resolve="properties" />
        <node concept="2EHx9g" id="2KIKjjWft51" role="2czzBx" />
        <node concept="VPM3Z" id="2KIKjjWft3M" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1guR3D" role="3EZMnx">
        <node concept="pVoyu" id="6P7bv1guR3E" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1guR3F" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1guR4n" role="3EZMnx">
        <node concept="pVoyu" id="6P7bv1guR4o" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1guR4p" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1gqwtB" role="3EZMnx">
        <property role="3F0ifm" value="nodes        " />
        <node concept="pVoyu" id="6P7bv1gqwtC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1gqwtD" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6P7bv1gt9E0" role="3EZMnx">
        <property role="2czwfO" value=" " />
        <ref role="1NtTu8" to="brs9:6P7bv1gqwsG" resolve="nodes" />
        <node concept="2EHx9g" id="6P7bv1gt9E1" role="2czzBx" />
        <node concept="VPM3Z" id="6P7bv1gt9E2" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="lj46D" id="6P7bv1gt9FC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6P7bv1gt9Dd" role="3EZMnx">
        <node concept="pVoyu" id="6P7bv1gt9De" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6P7bv1gt9Df" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="2303sVWgxeN" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1gqww1">
    <property role="3GE5qa" value="process.node.event.end" />
    <ref role="1XX52x" to="brs9:6P7bv1gqww0" resolve="NoneEndEvent" />
    <node concept="3EZMnI" id="6P7bv1gqwwg" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBFsa" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6P7bv1gqwwn" role="3EZMnx">
        <property role="3F0ifm" value="end" />
      </node>
      <node concept="3EZMnI" id="6P7bv1gqVA4" role="3EZMnx">
        <node concept="VPM3Z" id="6P7bv1gqVA5" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="6P7bv1guRlE" role="3EZMnx">
          <node concept="VPM3Z" id="6P7bv1guRlF" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="6P7bv1guRlG" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="6P7bv1guRlH" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="6P7bv1guRlI" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="6P7bv1guRlN" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="6P7bv1gqVAl" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="6P7bv1gqwwj" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1gqww4">
    <property role="3GE5qa" value="process.node.event.start" />
    <ref role="1XX52x" to="brs9:6P7bv1gqww3" resolve="NoneStartEvent" />
    <node concept="3EZMnI" id="6P7bv1gqww6" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBwA8" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6P7bv1gqwwd" role="3EZMnx">
        <property role="3F0ifm" value="start" />
      </node>
      <node concept="3EZMnI" id="6P7bv1gqTej" role="3EZMnx">
        <node concept="VPM3Z" id="6P7bv1gqTel" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="6P7bv1guRmb" role="3EZMnx">
          <node concept="VPM3Z" id="6P7bv1guRmc" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="6P7bv1guRmg" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="6aK4n5uAVdr" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="6aK4n5uAVd_" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="6P7bv1guRmk" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="6P7bv1gqV_i" role="3EZMnx">
          <node concept="VPM3Z" id="6P7bv1gqV_k" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="6P7bv1gqV_m" role="3EZMnx" />
          <node concept="3EZMnI" id="6P7bv1gqV_C" role="3EZMnx">
            <node concept="VPM3Z" id="6P7bv1gqV_E" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="6P7bv1gqTe$" role="3EZMnx">
              <node concept="VPM3Z" id="6P7bv1gqTeA" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="6P7bv1gqTeC" role="3EZMnx">
                <property role="3F0ifm" value="flows " />
              </node>
              <node concept="3F2HdR" id="6P7bv1gqTfm" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqwvY" resolve="outgoingFlows" />
                <node concept="2EHx9g" id="6P7bv1gqTfE" role="2czzBx" />
              </node>
              <node concept="2iRfu4" id="6P7bv1gqTeD" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="6P7bv1gqV_H" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="6P7bv1gqV_n" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="6P7bv1gqTeo" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="6P7bv1gqww9" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1gqQYW">
    <property role="3GE5qa" value="process.node.activity.task.user" />
    <ref role="1XX52x" to="brs9:6P7bv1gqQYV" resolve="UserTaskActivity" />
    <node concept="3EZMnI" id="1jWl1KghOYH" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBFqV" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1jWl1KghOYI" role="3EZMnx">
        <property role="3F0ifm" value="user task" />
      </node>
      <node concept="3EZMnI" id="1jWl1KghOYJ" role="3EZMnx">
        <node concept="VPM3Z" id="1jWl1KghOYK" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="1jWl1KghOYL" role="3EZMnx">
          <node concept="VPM3Z" id="1jWl1KghOYM" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="1jWl1KghOYN" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="1jWl1KghOYO" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="1jWl1KghOYP" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="1jWl1KghOYU" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="1jWl1KghOYV" role="3EZMnx">
          <node concept="VPM3Z" id="1jWl1KghOYW" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="1jWl1KghOYX" role="3EZMnx">
            <property role="3F0ifm" value="   " />
          </node>
          <node concept="3EZMnI" id="1jWl1KghOYY" role="3EZMnx">
            <node concept="VPM3Z" id="1jWl1KghOYZ" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="1jWl1KghOZj" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1KghOZk" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1KghOZl" role="3EZMnx">
                <property role="3F0ifm" value="flows      " />
              </node>
              <node concept="3F2HdR" id="1jWl1KghOZm" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqwvY" resolve="outgoingFlows" />
                <node concept="2EHx9g" id="1jWl1KghOZn" role="2czzBx" />
              </node>
              <node concept="2iRfu4" id="1jWl1KghOZo" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="1jWl1KghOZp" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="1jWl1KghOZq" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="1jWl1KghOZr" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="1jWl1KghOZs" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1gqTdI">
    <property role="3GE5qa" value="process.flow.sequence" />
    <ref role="1XX52x" to="brs9:6P7bv1gqTdH" resolve="SequenceFlow" />
    <node concept="3EZMnI" id="6P7bv1gqTdK" role="2wV5jI">
      <node concept="3EZMnI" id="6P7bv1grawE" role="3EZMnx">
        <node concept="VPM3Z" id="6P7bv1grawG" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="6flZoDGqlYa" role="3EZMnx">
          <node concept="VPM3Z" id="6flZoDGqlYc" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0A7n" id="6flZoDGqlYE" role="3EZMnx">
            <property role="39s7Ar" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
          <node concept="3F0ifn" id="6flZoDGqlYK" role="3EZMnx">
            <property role="3F0ifm" value="  &quot;" />
          </node>
          <node concept="3F0A7n" id="6flZoDGqlYS" role="3EZMnx">
            <property role="39s7Ar" value="true" />
            <ref role="1NtTu8" to="brs9:6flZoDGqlWI" resolve="caption" />
          </node>
          <node concept="3F0ifn" id="6flZoDGqlZ2" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="6flZoDGqlYf" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="6P7bv1grawV" role="3EZMnx">
          <node concept="VPM3Z" id="6P7bv1grawX" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="6P7bv1grawZ" role="3EZMnx">
            <property role="3F0ifm" value="  " />
          </node>
          <node concept="3EZMnI" id="6P7bv1graxa" role="3EZMnx">
            <node concept="VPM3Z" id="6P7bv1graxc" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="1jWl1Kgi3XK" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1Kgi3XL" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1Kgi3XM" role="3EZMnx">
                <property role="3F0ifm" value="-&gt;" />
              </node>
              <node concept="1iCGBv" id="1jWl1Kgi3XN" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqTdF" resolve="targetNode" />
                <node concept="1sVBvm" id="1jWl1Kgi3XO" role="1sWHZn">
                  <node concept="3F0A7n" id="1jWl1Kgi3XP" role="2wV5jI">
                    <property role="1Intyy" value="true" />
                    <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="2iRfu4" id="1jWl1Kgi3XQ" role="2iSdaV" />
            </node>
            <node concept="3EZMnI" id="6P7bv1graxl" role="3EZMnx">
              <node concept="VPM3Z" id="6P7bv1graxn" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="6P7bv1graxv" role="3EZMnx">
                <property role="3F0ifm" value=" ?" />
              </node>
              <node concept="3F2HdR" id="1jWl1Kgi3Yq" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:1jWl1Kgi3cf" resolve="conditions" />
                <node concept="2EHx9g" id="1jWl1Kgi3YB" role="2czzBx" />
                <node concept="VPM3Z" id="1jWl1Kgi3Yu" role="3F10Kt">
                  <property role="VOm3f" value="false" />
                </node>
              </node>
              <node concept="2iRfu4" id="6P7bv1graxq" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="6P7bv1graxf" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="6P7bv1grax0" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="6P7bv1grawJ" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="6P7bv1gqTdN" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1grfWb">
    <property role="3GE5qa" value="process.node.activity.task.service" />
    <ref role="1XX52x" to="brs9:6P7bv1grfW0" resolve="ExecuteServiceMethodServiceTaskActivity" />
    <node concept="3EZMnI" id="6P7bv1grik1" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBFnI" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6P7bv1grik2" role="3EZMnx">
        <property role="3F0ifm" value="service method" />
      </node>
      <node concept="3EZMnI" id="6P7bv1grik3" role="3EZMnx">
        <node concept="VPM3Z" id="6P7bv1grik4" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="6P7bv1gtiMC" role="3EZMnx">
          <node concept="VPM3Z" id="6P7bv1gtiMD" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="6P7bv1gtiME" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="6P7bv1gtiMF" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="6P7bv1gtiMG" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="6P7bv1gtiMH" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="6P7bv1grik6" role="3EZMnx">
          <node concept="VPM3Z" id="6P7bv1grik7" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="6P7bv1grik8" role="3EZMnx">
            <property role="3F0ifm" value="   " />
          </node>
          <node concept="3EZMnI" id="6P7bv1grik9" role="3EZMnx">
            <node concept="VPM3Z" id="6P7bv1grika" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="1jWl1Kgh588" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1Kgh589" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1Kgh58a" role="3EZMnx">
                <property role="3F0ifm" value="method  " />
              </node>
              <node concept="3F1sOY" id="1jWl1Kgh58b" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1grfW1" resolve="serviceMethod" />
              </node>
              <node concept="2iRfu4" id="1jWl1Kgh58c" role="2iSdaV" />
            </node>
            <node concept="3EZMnI" id="6P7bv1grikD" role="3EZMnx">
              <node concept="VPM3Z" id="6P7bv1grikE" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="6P7bv1grikF" role="3EZMnx">
                <property role="3F0ifm" value="flows   " />
              </node>
              <node concept="3F2HdR" id="6P7bv1grikG" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqwvY" resolve="outgoingFlows" />
                <node concept="2EHx9g" id="6P7bv1grikH" role="2czzBx" />
              </node>
              <node concept="2iRfu4" id="6P7bv1grikI" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="6P7bv1grikh" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="6P7bv1griki" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="6P7bv1grikj" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="6P7bv1grikk" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1guR3v">
    <property role="3GE5qa" value="process.swimlane.lane" />
    <ref role="1XX52x" to="brs9:6P7bv1guR3a" resolve="Lane" />
    <node concept="3F0A7n" id="6P7bv1guR3x" role="2wV5jI">
      <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
    </node>
  </node>
  <node concept="24kQdi" id="6P7bv1guV56">
    <property role="3GE5qa" value="process.swimlane.pool" />
    <ref role="1XX52x" to="brs9:6P7bv1guV4V" resolve="Pool" />
    <node concept="3EZMnI" id="6P7bv1guV58" role="2wV5jI">
      <node concept="3F0ifn" id="6P7bv1guV5f" role="3EZMnx">
        <property role="3F0ifm" value="Pool" />
      </node>
      <node concept="3F0A7n" id="6P7bv1guV5l" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="6P7bv1guV5b" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1jWl1Kgh59I">
    <property role="3GE5qa" value="process.node.activity.task.message" />
    <ref role="1XX52x" to="brs9:1jWl1Kgh59t" resolve="StringMessage" />
    <node concept="3EZMnI" id="1jWl1Kgh59K" role="2wV5jI">
      <node concept="3F0ifn" id="1jWl1Kgh59R" role="3EZMnx">
        <property role="3F0ifm" value="String message" />
      </node>
      <node concept="3F0A7n" id="1jWl1Kgh5a9" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="1jWl1Kgh59N" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1jWl1Kgh5ax">
    <property role="3GE5qa" value="process.node.activity.task.message.send" />
    <ref role="1XX52x" to="brs9:1jWl1Kgh59q" resolve="SendTaskActivity" />
    <node concept="3EZMnI" id="1jWl1Kgh5az" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBFlK" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1jWl1Kgh5a$" role="3EZMnx">
        <property role="3F0ifm" value="send" />
      </node>
      <node concept="3EZMnI" id="1jWl1Kgh5a_" role="3EZMnx">
        <node concept="VPM3Z" id="1jWl1Kgh5aA" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="1jWl1Kgh5aB" role="3EZMnx">
          <node concept="VPM3Z" id="1jWl1Kgh5aC" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5aD" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="1jWl1Kgh5aE" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5aF" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5aG" role="3EZMnx">
            <property role="3F0ifm" value=" " />
          </node>
          <node concept="l2Vlx" id="1jWl1Kgh5aK" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="1jWl1Kgh5aL" role="3EZMnx">
          <node concept="VPM3Z" id="1jWl1Kgh5aM" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5aN" role="3EZMnx">
            <property role="3F0ifm" value="   " />
          </node>
          <node concept="3EZMnI" id="1jWl1Kgh5aO" role="3EZMnx">
            <node concept="VPM3Z" id="1jWl1Kgh5aP" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="1jWl1Kgh5bM" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1Kgh5bN" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1Kgh5cy" role="3EZMnx">
                <property role="3F0ifm" value="message    " />
              </node>
              <node concept="1iCGBv" id="1jWl1Kgh5cC" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:1jWl1Kghv$J" resolve="message" />
                <node concept="1sVBvm" id="1jWl1Kgh5cE" role="1sWHZn">
                  <node concept="3F0A7n" id="1jWl1Kgh5cM" role="2wV5jI">
                    <property role="1Intyy" value="true" />
                    <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="2iRfu4" id="1jWl1Kgh5bP" role="2iSdaV" />
            </node>
            <node concept="3EZMnI" id="1jWl1Kgh5c7" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1Kgh5c8" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1Kgh5c9" role="3EZMnx">
                <property role="3F0ifm" value="to process " />
              </node>
              <node concept="1iCGBv" id="1jWl1Kgh5cT" role="3EZMnx">
                <property role="39s7Ar" value="true" />
                <ref role="1NtTu8" to="brs9:1jWl1Kgh5bD" resolve="targetProcess" />
                <node concept="1sVBvm" id="1jWl1Kgh5cV" role="1sWHZn">
                  <node concept="3F0A7n" id="1jWl1Kgh5d3" role="2wV5jI">
                    <property role="1Intyy" value="true" />
                    <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="2iRfu4" id="1jWl1Kgh5ca" role="2iSdaV" />
            </node>
            <node concept="3EZMnI" id="1jWl1Kgh5aV" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1Kgh5aW" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1Kgh5aX" role="3EZMnx">
                <property role="3F0ifm" value="flows      " />
              </node>
              <node concept="3F2HdR" id="1jWl1Kgh5aY" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqwvY" resolve="outgoingFlows" />
                <node concept="2EHx9g" id="1jWl1Kgh5aZ" role="2czzBx" />
              </node>
              <node concept="2iRfu4" id="1jWl1Kgh5b0" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="1jWl1Kgh5b1" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="1jWl1Kgh5b2" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="1jWl1Kgh5b3" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="1jWl1Kgh5b4" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1jWl1Kgh5hA">
    <property role="3GE5qa" value="process.node.activity.task.message.receive" />
    <ref role="1XX52x" to="brs9:1jWl1Kgh5h$" resolve="ReceiveTaskActivity" />
    <node concept="3EZMnI" id="1jWl1Kgh5hC" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBFk1" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1jWl1Kgh5hD" role="3EZMnx">
        <property role="3F0ifm" value="receive" />
      </node>
      <node concept="3EZMnI" id="1jWl1Kgh5hE" role="3EZMnx">
        <node concept="VPM3Z" id="1jWl1Kgh5hF" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="1jWl1Kgh5hG" role="3EZMnx">
          <node concept="VPM3Z" id="1jWl1Kgh5hH" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5hI" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="1jWl1Kgh5hJ" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5hK" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="1jWl1Kgh5hP" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="1jWl1Kgh5hQ" role="3EZMnx">
          <node concept="VPM3Z" id="1jWl1Kgh5hR" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="1jWl1Kgh5hS" role="3EZMnx">
            <property role="3F0ifm" value="   " />
          </node>
          <node concept="3EZMnI" id="1jWl1Kgh5hT" role="3EZMnx">
            <node concept="VPM3Z" id="1jWl1Kgh5hU" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="1jWl1KghTng" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1KghTnh" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1KghTni" role="3EZMnx">
                <property role="3F0ifm" value="message    " />
              </node>
              <node concept="1iCGBv" id="1jWl1KghTnj" role="3EZMnx">
                <property role="39s7Ar" value="true" />
                <ref role="1NtTu8" to="brs9:1jWl1Kghv$J" resolve="message" />
                <node concept="1sVBvm" id="1jWl1KghTnk" role="1sWHZn">
                  <node concept="3F0A7n" id="1jWl1KghTnl" role="2wV5jI">
                    <property role="1Intyy" value="true" />
                    <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="2iRfu4" id="1jWl1KghTnm" role="2iSdaV" />
            </node>
            <node concept="3EZMnI" id="1jWl1Kgh5ie" role="3EZMnx">
              <node concept="VPM3Z" id="1jWl1Kgh5if" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="1jWl1Kgh5ig" role="3EZMnx">
                <property role="3F0ifm" value="flows      " />
              </node>
              <node concept="3F2HdR" id="1jWl1Kgh5ih" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqwvY" resolve="outgoingFlows" />
                <node concept="2EHx9g" id="1jWl1Kgh5ii" role="2czzBx" />
              </node>
              <node concept="2iRfu4" id="1jWl1Kgh5ij" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="1jWl1Kgh5ik" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="1jWl1Kgh5il" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="1jWl1Kgh5im" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="1jWl1Kgh5in" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1jWl1KghXTs">
    <property role="3GE5qa" value="process.flow.condition.eval" />
    <ref role="1XX52x" to="brs9:1jWl1KghXTf" resolve="JavaScriptEvalFlowCondition" />
    <node concept="3EZMnI" id="1jWl1KghXTu" role="2wV5jI">
      <node concept="3F0ifn" id="1jWl1KghXTC" role="3EZMnx">
        <property role="3F0ifm" value="javascipt eval : " />
      </node>
      <node concept="3F0A7n" id="1jWl1KghXTL" role="3EZMnx">
        <ref role="1NtTu8" to="brs9:1jWl1KghXTh" resolve="expression" />
      </node>
      <node concept="2iRfu4" id="1jWl1KghXTx" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1jWl1KgihZw">
    <property role="3GE5qa" value="process.flow.condition.message" />
    <ref role="1XX52x" to="brs9:1jWl1KgihZk" resolve="ReceiveMessageFlowCondition" />
    <node concept="3EZMnI" id="1jWl1KgihZy" role="2wV5jI">
      <node concept="3F0ifn" id="1jWl1KgihZG" role="3EZMnx">
        <property role="3F0ifm" value="receive message" />
      </node>
      <node concept="1iCGBv" id="1jWl1KgihZP" role="3EZMnx">
        <ref role="1NtTu8" to="brs9:1jWl1KgihZl" resolve="message" />
        <node concept="1sVBvm" id="1jWl1KgihZR" role="1sWHZn">
          <node concept="3F0A7n" id="1jWl1Kgii03" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="1jWl1KgihZ_" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="52sT7wuT9om">
    <property role="3GE5qa" value="process.flow.condition.service" />
    <ref role="1XX52x" to="brs9:52sT7wuT9ob" resolve="ServiceMethodFlowCondition" />
    <node concept="3EZMnI" id="52sT7wuT9oo" role="2wV5jI">
      <node concept="3F0ifn" id="52sT7wuT9op" role="3EZMnx">
        <property role="3F0ifm" value="service method" />
      </node>
      <node concept="3F1sOY" id="52sT7wuT9oC" role="3EZMnx">
        <ref role="1NtTu8" to="brs9:52sT7wuT9oc" resolve="serviceMethod" />
      </node>
      <node concept="2iRfu4" id="52sT7wuT9ot" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="UIK3MHoedq">
    <property role="3GE5qa" value="process.node.activity.task.service" />
    <ref role="1XX52x" to="brs9:UIK3MHoed8" resolve="SetFieldValueServiceTaskActivity" />
    <node concept="3EZMnI" id="UIK3MHoeds" role="2wV5jI">
      <node concept="3F0A7n" id="6aK4n5uBFpl" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="UIK3MHoedt" role="3EZMnx">
        <property role="3F0ifm" value="set field value" />
      </node>
      <node concept="3EZMnI" id="UIK3MHoedu" role="3EZMnx">
        <node concept="VPM3Z" id="UIK3MHoedv" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="UIK3MHoedw" role="3EZMnx">
          <node concept="VPM3Z" id="UIK3MHoedx" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="UIK3MHoedy" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="3F0A7n" id="UIK3MHoedz" role="3EZMnx">
            <ref role="1NtTu8" to="brs9:6P7bv1guYFG" resolve="description" />
          </node>
          <node concept="3F0ifn" id="UIK3MHoed$" role="3EZMnx">
            <property role="3F0ifm" value="&quot;" />
          </node>
          <node concept="l2Vlx" id="UIK3MHoedD" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="UIK3MHoedE" role="3EZMnx">
          <node concept="VPM3Z" id="UIK3MHoedF" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="UIK3MHoedG" role="3EZMnx">
            <property role="3F0ifm" value="   " />
          </node>
          <node concept="3EZMnI" id="UIK3MHoedH" role="3EZMnx">
            <node concept="VPM3Z" id="UIK3MHoedI" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="3EZMnI" id="UIK3MHoedJ" role="3EZMnx">
              <node concept="VPM3Z" id="UIK3MHoedK" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="2iRfu4" id="UIK3MHoedN" role="2iSdaV" />
              <node concept="3F0ifn" id="UIK3MHoefh" role="3EZMnx">
                <property role="3F0ifm" value="field   " />
              </node>
              <node concept="1iCGBv" id="UIK3MHoefp" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:UIK3MHoedc" resolve="typeField" />
                <node concept="1sVBvm" id="UIK3MHoefr" role="1sWHZn">
                  <node concept="3F0A7n" id="UIK3MHoefB" role="2wV5jI">
                    <property role="1Intyy" value="true" />
                    <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="3F0ifn" id="UIK3MHoefQ" role="3EZMnx">
                <property role="3F0ifm" value="=" />
              </node>
              <node concept="3F0A7n" id="UIK3MHoegf" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:UIK3MHoedf" resolve="value" />
              </node>
            </node>
            <node concept="3EZMnI" id="UIK3MHoedT" role="3EZMnx">
              <node concept="VPM3Z" id="UIK3MHoedU" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="3F0ifn" id="UIK3MHoedV" role="3EZMnx">
                <property role="3F0ifm" value="flows   " />
              </node>
              <node concept="3F2HdR" id="UIK3MHoedW" role="3EZMnx">
                <ref role="1NtTu8" to="brs9:6P7bv1gqwvY" resolve="outgoingFlows" />
                <node concept="2EHx9g" id="UIK3MHoedX" role="2czzBx" />
              </node>
              <node concept="2iRfu4" id="UIK3MHoedY" role="2iSdaV" />
            </node>
            <node concept="2iRkQZ" id="UIK3MHoedZ" role="2iSdaV" />
          </node>
          <node concept="l2Vlx" id="UIK3MHoee0" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="UIK3MHoee1" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="UIK3MHoee2" role="2iSdaV" />
    </node>
  </node>
</model>

