/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.storage.process;


import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.storage.processnode.ProcessNodeService;
import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessService extends ProcessServicePrototype {

    private static ConcurrentHashMap<String, BigDecimal> _fullNamesCache = new ConcurrentHashMap<>();

    @SuppressWarnings("Duplicates")
    public BigDecimal fullNameToId(String name) {
        if ( name == null ) {
            return null;
        }
        String[] tokens = StringUtils.splitByLastTokenOccurrence(name, "\\.");
        if ( tokens.length < 2 ) {
            return null;
        }
        BigDecimal id = packageNameToId(tokens[0], tokens[1]);
        if ( id != null ) {
            _fullNamesCache.put(name, id);
        }
        return id;
    }

    private static ConcurrentHashMap<BigDecimal, Process> _processesCache = new ConcurrentHashMap<>();

    @NotInitializeService
    private MetadataBase _metadataBase;

    public MetadataBase getMetadataBase() {
        if ( _metadataBase == null ) {
            _metadataBase = getService(MetadataBase.class);
        }
        return _metadataBase;
    }

    @NotInitializeService
    ProcessNodeService _processNodeService;

    public ProcessNodeService getProcessNodeService() {
        if ( _processNodeService == null ) {
            _processNodeService = getService(ProcessNodeService.class);
        }
        return _processNodeService;
    }

    public Process getProcess(BigDecimal processId) {
        if ( processId == null ) {
            return null;
        }
        Process process = _processesCache.get(processId);
        if ( process != null ){
            return process;
        }
        ProcessObject processObject = load(processId);
        if ( processObject == null ) {
            throwException("Process with id ? not found in database", processId);
        }

        process = getMetadataBase().getNode(Process.class, processObject.getPackageName(), processObject.getName(), true);
        process.setId(processId);

        for (AbstractProcessNode processNode : process.getNodes() ) {
            BigDecimal id = getProcessNodeService().processNameToId(processId, processNode.getName());
            if ( id == null ) {
                throwException("Process ? node '?' not registered in database. Attempt to reinstall module", process.getFullName(), processNode.getName());
            }
            processNode.setId(id);
        }

        _processesCache.put(processId, process);
        return process;
    }

    public static void clearStaticCaches() {
        _fullNamesCache.clear();
        _processesCache.clear();
    }

    @Override
    public void save(ProcessObject processObject) {
        super.save(processObject);
        clearStaticCaches();
    }

    @Override
    public void delete(BigDecimal id) {
        super.delete(id);
        clearStaticCaches();
    }
}
