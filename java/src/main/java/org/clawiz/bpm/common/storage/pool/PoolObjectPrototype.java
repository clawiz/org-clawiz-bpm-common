package org.clawiz.bpm.common.storage.pool;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class PoolObjectPrototype extends AbstractObject {
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    public PoolService service;
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Pool.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     Pool object
    */
    public PoolObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Pool.Name", "128");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     Pool object
    */
    public PoolObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    public PoolService getService() {
        return this.service;
    }
    
    public void setService(PoolService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(PoolObjectPrototype  target) {
        target.setPackageName(getPackageName());
        target.setName(getName());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((PoolObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((PoolObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(PoolObjectPrototype object) {
        return 
               isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'PackageName' fields
    * 
    * @return     Concatenated string values of key 'toPackageName' fields
    */
    public String toPackageName() {
        return getPackageName() + "," + getName();
    }
}
