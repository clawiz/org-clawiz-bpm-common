/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.install;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.generator.service.ProcessGenerator;
import org.clawiz.bpm.common.storage.process.ProcessObject;
import org.clawiz.bpm.common.storage.process.ProcessService;
import org.clawiz.bpm.common.storage.processnode.ProcessNodeObject;
import org.clawiz.bpm.common.storage.processnode.ProcessNodeService;
import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.storage.type.TypeService;

import java.math.BigDecimal;

public class ProcessInstaller extends AbstractMetadataNodeInstaller {

    ProcessService            processService;
    ProcessNodeService        processNodeService;
    TypeService               typeService;

    protected void saveProcess(Process process) {

        BigDecimal processId = processService.packageNameToId(process.getPackageName(), process.getName());

        ProcessObject po = processId == null ? processService.create() : processService.load(processId);
        po.setPackageName(process.getPackageName());
        po.setName(process.getName());
        po.setRootTypeId(process.getRootType() != null ? typeService.fullNameToId(process.getRootType().getFullName()) : null);
        po.save();

        for ( AbstractProcessNode processNode : process.getNodes() ) {
            ProcessNodeObject nodeObject = processNodeService.load(processNodeService.processNameToId(po.getId(),  processNode.getName(), true));
            nodeObject.save();
        }

    }

    @Override
    public void process() {

        saveProcess((Process) getNode());

        ProcessGenerator generator = getService(ProcessGenerator.class, true);

        generator.setRootDestinationPath(getDestinationPath());
        generator.setProcess((Process) getNode());

        generator.run();

    }
}
