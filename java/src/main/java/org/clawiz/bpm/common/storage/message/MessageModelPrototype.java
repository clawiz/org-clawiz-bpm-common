package org.clawiz.bpm.common.storage.message;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class MessageModelPrototype extends TypeModel {
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static Type type;
    
    private MessageService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public MessageService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(MessageService.class).getType();
        
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Message.PackageName"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Message.Name"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PACKAGENAME" : return packageName();
        case "NAME" : return name();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
