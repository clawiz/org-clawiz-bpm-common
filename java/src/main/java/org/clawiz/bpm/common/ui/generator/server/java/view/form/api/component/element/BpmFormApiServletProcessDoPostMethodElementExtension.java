/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.ui.generator.server.java.view.form.api.component.element;

import org.clawiz.bpm.common.ui.generator.server.java.view.form.component.element.AbstractBpmFormServletMethodElementExtension;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.service.extension.ServiceExtensionMethod;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.FormApiServletProcessDoPostMethodElement;

public class BpmFormApiServletProcessDoPostMethodElementExtension extends AbstractBpmFormServletMethodElementExtension {

    public static final String PROCESS_EXECUTE_PROCESS_FLOW_METHOD_NAME = "processExecuteProcessFlow";
    public static final String EXECUTE_PROCESS_FLOW_METHOD_NAME         = "executeProcessFlow";

    @ServiceExtensionMethod(name = FormApiServletProcessDoPostMethodElement.PROCESS_DO_POST_EXTENSION_NAME)
    public void processDoPost() {

        if ( getForm().getRootType() == null
                || processGeneratorHelper.getProcessesByRootType(getForm().getRootType()).size() == 0 ) {
            return;
        }


        JavaMethodElement processExecuteMethod = getMethodElement().getComponent().addMethod(PROCESS_EXECUTE_PROCESS_FLOW_METHOD_NAME);
        processExecuteMethod.addParameter(getMethodElement().getRequestContextClassName(), "request");
        processExecuteMethod.addText("");
        processExecuteMethod.addText("processManager.executeFlow(");
        processExecuteMethod.addText("        request.getId()");
        processExecuteMethod.addText("        ,request.getProcessName()");
        processExecuteMethod.addText("        ,request.getProcessNodeName()");
        processExecuteMethod.addText("        ,request.getProcessFlowName()");
        processExecuteMethod.addText(");");
        processExecuteMethod.addText("");
//        processRunMethod.addText("bpmProcessManager.runRule(request.getId(), request.getBpmProcessName(), request.getBpmProcessRuleName());");

        JavaMethodElement executeProcessFlow = getMethodElement().getComponent().addMethod(getMethodElement().getResponseContextClassName(), EXECUTE_PROCESS_FLOW_METHOD_NAME);
        executeProcessFlow.addParameter(getMethodElement().getRequestContextClassName(), "request");
        executeProcessFlow.addText("save(request);");
        executeProcessFlow.addText(PROCESS_EXECUTE_PROCESS_FLOW_METHOD_NAME + "(request);");
        executeProcessFlow.addText("return load(request);");
        executeProcessFlow.addText("");

        addText("if ( \"executeProcessFlow\".equals(request.getAction()) ) { return " + EXECUTE_PROCESS_FLOW_METHOD_NAME + "(request); }" );
        addText("");

    }
}
