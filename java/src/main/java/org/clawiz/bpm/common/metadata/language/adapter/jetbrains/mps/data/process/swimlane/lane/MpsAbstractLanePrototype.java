package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractLanePrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7111202990234466609";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractLane";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.swimlane.lane.AbstractLane.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.swimlane.lane.AbstractLane structure = (org.clawiz.bpm.common.metadata.data.process.swimlane.lane.AbstractLane) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.swimlane.lane.AbstractLane structure = (org.clawiz.bpm.common.metadata.data.process.swimlane.lane.AbstractLane) node;
        
    }
}
