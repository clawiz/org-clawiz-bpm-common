/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.metadata.data.process;

import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.metadata.data.process.node.ProcessNodeList;
import org.clawiz.bpm.common.metadata.data.process.node.event.start.NoneStartEvent;
import org.clawiz.bpm.common.metadata.data.process.swimlane.lane.LaneList;
import org.clawiz.bpm.common.install.ProcessInstaller;
import org.clawiz.bpm.common.manager.instance.AbstractProcessInstance;
import org.clawiz.bpm.common.manager.service.AbstractProcessService;
import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.data.type.Type;

public class Process extends ProcessPrototype {

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) ProcessInstaller.class;
    }

    private String _servicePackageName;
    public String getServicePackageName() {
        if ( _servicePackageName == null ) {
            _servicePackageName = getPackageName() + "." + getJavaName().toLowerCase();
        }
        return _servicePackageName;
    }

    @Override
    public String getJavaClassName() {
        return super.getJavaClassName();
    }

    private String _javaClassNamePrefix;
    private String getJavaClassNamePrefix() {
        if ( _javaClassNamePrefix != null ) {
            return _javaClassNamePrefix;
        }
        _javaClassNamePrefix = getServicePackageName() + "." + GeneratorUtils.toJavaName(getName(), true);
        return _javaClassNamePrefix;
    }

    String _instanceClassName;
    public String getInstanceClassName() {
        if ( _instanceClassName == null ) {
            _instanceClassName = getJavaClassName() + "Instance";
        }
        return _instanceClassName;
    }

    private String _serviceClassName;
    public String getServiceClassName() {
        if ( _serviceClassName == null ) {
            _serviceClassName = getJavaClassName() + "Service";
        }
        return _serviceClassName;
    }

    private Class<AbstractProcessInstance> _instanceClass;
    public Class<AbstractProcessInstance> getInstanceClass() {
        if ( _instanceClass != null ) {
            return _instanceClass;
        }
        _instanceClass = Core.getClassByName(getServicePackageName() + "." + getInstanceClassName());
        return _instanceClass;
    }

    private Class<AbstractProcessService> _serviceClass;
    public Class<AbstractProcessService> getServiceClass() {
        if ( _serviceClass != null ) {
            return _serviceClass;
        }
        _serviceClass = Core.getClassByName(getServicePackageName() + "." + getServiceClassName());
        return _serviceClass;
    }

    private ProcessNodeList _startNodes;

    public ProcessNodeList getStartNodes() {

        if ( _startNodes == null ) {
            ProcessNodeList startNodes = new ProcessNodeList();
            AbstractProcessNode startNode = null;
            for (AbstractProcessNode node : getNodes() ) {
                if (NoneStartEvent.class.isAssignableFrom(node.getClass())) {
                    if ( startNode != null ) {
                        throw new CoreException("Process ? have more then one NoneStartEvent node", getFullName());
                    }
                    startNode = node;
                }
            }
            if ( startNode == null ) {
                throw new CoreException("Process ? have not NoneStartEvent node", getFullName());
            }

            startNodes.add(startNode);
            _startNodes = startNodes;

        }

        return _startNodes;
    }



}
