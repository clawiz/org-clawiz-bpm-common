package org.clawiz.bpm.common.generator.service.component.service.element.flow.condition;

import org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition;

public class ProcessServiceServiceMethodFlowConditionJavaElement extends AbstractProcessServicePrototypeFlowConditionJavaElement {

    @Override
    public void process() {
        super.process();

        ServiceMethodFlowCondition condition = (ServiceMethodFlowCondition) getCondition();

        String serviceClassName = condition.getServiceMethod().getService().getFullName();

        getComponent().addImport(serviceClassName);
        addText("if ( ! getService(" + serviceClassName + ".class)." + condition.getServiceMethod().getName() + "(instance) ) { return false; }");


    }
}
