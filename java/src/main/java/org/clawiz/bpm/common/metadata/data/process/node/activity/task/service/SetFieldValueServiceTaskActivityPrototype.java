package org.clawiz.bpm.common.metadata.data.process.node.activity.task.service;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class SetFieldValueServiceTaskActivityPrototype extends org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.AbstractServiceTaskActivity {
    
    @ExchangeAttribute
    private String value;
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.field.TypeField typeField;
    
    public SetFieldValueServiceTaskActivity withName(String value) {
        setName(value);
        return (SetFieldValueServiceTaskActivity) this;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public SetFieldValueServiceTaskActivity withValue(String value) {
        setValue(value);
        return (SetFieldValueServiceTaskActivity) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeField getTypeField() {
        return this.typeField;
    }
    
    public void setTypeField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        this.typeField = value;
    }
    
    public SetFieldValueServiceTaskActivity withTypeField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        setTypeField(value);
        return (SetFieldValueServiceTaskActivity) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getTypeField() != null ) { 
            getTypeField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getTypeField());
        
    }
}
