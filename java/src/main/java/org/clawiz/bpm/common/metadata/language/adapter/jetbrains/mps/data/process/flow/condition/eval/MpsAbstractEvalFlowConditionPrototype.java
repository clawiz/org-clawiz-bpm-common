package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.eval;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractEvalFlowConditionPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition {
    
    public String expression;
    
    public String getExpression() {
        return this.expression;
    }
    
    public void setExpression(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.expression = null;
        }
        this.expression = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1512176054398606928";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractEvalFlowCondition";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "1512176054398606928", "org.clawiz.bpm.common.language.structure.AbstractEvalFlowCondition", ConceptPropertyType.PROPERTY, "1512176054398606929", "expression"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("1512176054398606928", "expression", getExpression());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.AbstractEvalFlowCondition.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.AbstractEvalFlowCondition structure = (org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.AbstractEvalFlowCondition) node;
        
        structure.setExpression(getExpression());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.AbstractEvalFlowCondition structure = (org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.AbstractEvalFlowCondition) node;
        
        setExpression(structure.getExpression());
        
    }
}
