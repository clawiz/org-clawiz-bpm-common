package org.clawiz.bpm.common.metadata.data.process.node.activity.task.service;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ExecuteServiceMethodServiceTaskActivityPrototype extends org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.AbstractServiceTaskActivity {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.service.method.ServiceMethod serviceMethod;
    
    public ExecuteServiceMethodServiceTaskActivity withName(String value) {
        setName(value);
        return (ExecuteServiceMethodServiceTaskActivity) this;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.ServiceMethod getServiceMethod() {
        return this.serviceMethod;
    }
    
    public void setServiceMethod(org.clawiz.core.common.metadata.data.service.method.ServiceMethod value) {
        this.serviceMethod = value;
    }
    
    public ExecuteServiceMethodServiceTaskActivity withServiceMethod(org.clawiz.core.common.metadata.data.service.method.ServiceMethod value) {
        setServiceMethod(value);
        return (ExecuteServiceMethodServiceTaskActivity) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getServiceMethod() != null ) { 
            getServiceMethod().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getServiceMethod());
        
    }
}
