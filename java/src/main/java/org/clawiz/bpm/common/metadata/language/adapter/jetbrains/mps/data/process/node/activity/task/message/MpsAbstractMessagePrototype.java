package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractMessagePrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1512176054398374492";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractMessage";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage) node;
        
    }
}
