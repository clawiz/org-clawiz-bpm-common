package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps;

import java.lang.String;

public class MpsLanguageContextPrototype extends org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext {
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public void prepare() {
        
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.MpsProcess.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.property.MpsAbstractProcessProperty.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.event.MpsAbstractEvent.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.MpsAbstractFlow.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.gateway.MpsAbstractGateway.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.MpsAbstractActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.event.start.MpsAbstractStartEvent.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.event.end.MpsAbstractEndEvent.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.MpsAbstractTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service.MpsAbstractServiceTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.event.end.MpsNoneEndEvent.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.event.start.MpsNoneStartEvent.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.user.MpsAbstractUserTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.user.MpsUserTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.sequence.MpsSequenceFlow.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service.MpsExecuteServiceMethodServiceTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.sequence.MpsAbstractSequenceFlow.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.pool.MpsPool.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.send.MpsAbstractSendTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.send.MpsSendTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsAbstractMessage.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsStringMessage.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.receive.MpsAbstractReceiveTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.receive.MpsReceiveTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsAbstractMessageTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.eval.MpsJavaScriptEvalFlowCondition.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.eval.MpsAbstractEvalFlowCondition.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.message.MpsReceiveMessageFlowCondition.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.service.MpsServiceMethodFlowCondition.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service.MpsSetFieldValueServiceTaskActivity.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsAbstractLane.class);
        addClassMap(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.pool.MpsAbstractPool.class);
        
    }
}
