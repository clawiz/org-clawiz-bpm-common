/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service.component;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.generator.helper.ProcessGeneratorHelper;
import org.clawiz.bpm.common.generator.service.ProcessGenerator;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.StringUtils;

public class AbstractProcessServiceGeneratorJavaClassComponent extends AbstractJavaClassComponent {

    ProcessGenerator _processGenerator;
    Process                process;
    protected ProcessGeneratorHelper processGeneratorHelper;

    @Override
        public ProcessGenerator getGenerator() {
            return _processGenerator;
        }

        @Override
        public void setGenerator(AbstractGenerator generator) {
            super.setGenerator(generator);
            _processGenerator = (ProcessGenerator) generator;
            process           = _processGenerator.getProcess();
        }

        String _processJavaName;
        public String getProcessJavaName() {
            if ( _processJavaName != null ) {
                return _processJavaName;
            }
            _processJavaName = StringUtils.toUpperFirstChar(getProcess().getJavaName());
            return _processJavaName;
        }

        public String getServiceClassName() {
            return getProcessJavaName() + "Service";
        }

        public String getInstanceClassName() {
            return getProcessJavaName() + "Instance";
        }

        public Process getProcess() {
            return process;
        }

        public Type getRootType() {
            return process.getRootType();
        }

        public String getRootTypeObjectClassName() {
            return process.getRootType().getJavaClassName() + "Object";
        }

}
