/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.helper;

import org.clawiz.bpm.common.metadata.data.process.ProcessList;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.manager.ProcessManager;
import org.clawiz.bpm.common.manager.instance.AbstractProcessInstance;
import org.clawiz.bpm.common.manager.instance.ProcessInstanceList;
import org.clawiz.bpm.common.storage.process.ProcessService;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.metadata.data.type.Type;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessHelper extends Service {

    ObjectService  objectService;

    @NotInitializeService
    ProcessManager _processManager;

    @NotInitializeService
    ProcessService _processService;

    TypeService    typeService;

    public ProcessManager getProcessManager() {
        if ( _processManager == null ) {
            _processManager = getService(ProcessManager.class);
        }
        return _processManager;
    }

    public ProcessService getProcessService() {
        if ( _processService == null ) {
            _processService = getService(ProcessService.class);
        }
        return _processService;
    }

    private static ConcurrentHashMap<BigDecimal, ProcessList> processesByRootTypeCache = new ConcurrentHashMap<>();
    public ProcessList getProcessesByRootType(Type type) {

        if ( type == null ) {
            throwException("Cannot get processes list for null root type");
        }

        BigDecimal typeId = typeService.packageNameToId(type.getPackageName(), type.getName());
        if ( typeId == null ) {
            throwException("Type ?.? not found in database", type.getPackageName(), type.getName());
        }
        return getProcessesByRootType(typeId);
    }

    public ProcessList getProcessesByRootType(BigDecimal typeId) {

        if ( typeId == null ) {
            throwException("Cannot get processes list for null root type id");
        }

        ProcessList result = processesByRootTypeCache.get(typeId);
        if ( result != null ) {
            return result;
        }
        result = new ProcessList();

        Statement statement = executeQuery("select id from cw_bpm_processes where root_type_id = ?", typeId);
        while ( statement.next() ) {
            result.add(getProcessService().getProcess(statement.getBigDecimal(1)));
        }

        processesByRootTypeCache.put(typeId, result);

        return result;
    }

    public void dump(AbstractProcessInstance instance) {
        logDebug("<instance " + instance.getId() + ">");

        logDebug(" process    : " + instance.getProcess().getFullName());

        StringBuilder sb     = new StringBuilder();
        String        prefix = "";
        for(AbstractProcessNode state : instance.getNodes()) {
            sb.append(prefix).append(state.getName());
            prefix = ", ";
        }
        logDebug(" nodes      : " + sb.toString());
        prefix = " next flows : ";
/*
        for (ProcessRule rule : getProcessManager().getNextAllowedRules(instance)) {

            StringBuilder startStates = new StringBuilder();
            String        statePrefix = "";
            for (ProcessState state : rule.getStartStates() ) {
                startStates.append(statePrefix).append(state.getName());
                statePrefix = ", ";
            }
            StringBuilder endStates   = new StringBuilder();
            statePrefix = "";
            for (ProcessState state : rule.getEndStates() ) {
                endStates.append(statePrefix).append(state.getName());
                statePrefix = ", ";
            }

            logDebug(prefix + rule.getName() + "  ( " + startStates.toString() + " ) -> ( " + endStates.toString() + ")");
            prefix = "              ";
        }
*/


        logDebug("</process>");

    }

    public void dump(ProcessInstanceList instances) {
        instances.forEach(this::dump);
    }

    public void dumpObjectInstances(BigDecimal objectId) {
        logDebug("");
        logDebug("Start dump instances for " + objectId + " : " + objectService.idToString(objectId));
        dump(getProcessManager().getObjectInstanceList(objectId));
        logDebug("End   dump instances for " + objectId + " : " + objectService.idToString(objectId));
        logDebug("");
    }

}
