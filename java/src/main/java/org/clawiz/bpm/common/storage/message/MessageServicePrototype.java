package org.clawiz.bpm.common.storage.message;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class MessageServicePrototype extends AbstractTypeService<MessageObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.bpm.common.storage", "Message");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Message"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      message Checked object
    */
    public void check(MessageObject message) {
        
        if ( message == null ) {
            throwException("Cannot check null ?", new Object[]{"MessageObject"});
        }
        
        message.fillDefaults();
        
        
        if ( packageNameToId(message.getPackageName(), message.getName(), message.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Message", message.toPackageName() });
        }
        
    }
    
    /**
    * Create new MessageObject instance and fill default values
    * 
    * @return     Created object
    */
    public MessageObject create() {
        
        MessageObject message = new MessageObject();
        message.setService((MessageService) this);
        
        message.fillDefaults();
        
        return message;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public MessageObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, name from cw_bpm_messages where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Message", id});
        }
        
        MessageObject result = new MessageObject();
        
        result.setService((MessageService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setName(statement.getString(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of MessageObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public MessageList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of MessageObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public MessageList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        MessageList result = new MessageList();
        
        
        Statement statement = executeQuery("select id, package_name, name from cw_bpm_messages"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            MessageObject object = new MessageObject();
        
            object.setService((MessageService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setName(statement.getString(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'PackageName' fields
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name) {
        return packageNameToId(packageName, name, null);
    }
    
    /**
    * Find id of record by key 'PackageName' fields with id not equal skipId
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, BigDecimal skipId) {
        
        if ( packageName == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_bpm_messages where upper_package_name = ? and upper_name = ?", packageName.toUpperCase(), name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_bpm_messages where upper_package_name = ? and upper_name = ? and id != ?", packageName.toUpperCase(), name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'PackageName' fields or create new record with values set to given parameters
    * 
    * @param      packageName         PackageName
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = packageNameToId(packageName, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MessageObject object = create();
        object.setPackageName(packageName);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'PackageName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToPackageName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select package_name, name from cw_bpm_messages where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MessageObject object = new MessageObject();
        object.setService((MessageService)this);
        object.setId(id);
        object.setPackageName(statement.getString(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toPackageName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToPackageName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      messageObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(MessageObject messageObject) {
        return messageObject.toPackageName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, MessageObject oldMessageObject, MessageObject newMessageObject) {
        
        MessageObject o = oldMessageObject != null ? oldMessageObject : new MessageObject();
        MessageObject n = newMessageObject != null ? newMessageObject : new MessageObject();
        
        
        executeUpdate("insert into a_cw_bpm_messages (scn, action_type, id , o_package_name, o_upper_package_name, o_name, o_upper_name, n_package_name, n_upper_package_name, n_name, n_upper_name) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      message Saved object
    */
    public void save(MessageObject message) {
        
        if ( message == null ) {
            throwException("Cannot save NULL ?", new Object[]{"MessageObject"});
        }
        
        TransactionAction transactionAction;
        MessageObject oldMessage;
        if ( message.getService() == null ) {
            message.setService((MessageService)this);
        }
        
        check(message);
        
        if ( message.getId() == null ) {
        
            message.setId(getObjectService().createObject(getTypeId()));
        
            oldMessage = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, message.getId());
        
            executeUpdate("insert into cw_bpm_messages" 
                          + "( id, package_name, upper_package_name, name, upper_name ) "
                          + "values"
                          + "(?, ?, ?, ?, ?)",
                          message.getId(), message.getPackageName(), ( message.getPackageName() != null ? message.getPackageName().toUpperCase() : null ), message.getName(), ( message.getName() != null ? message.getName().toUpperCase() : null ) );
        
        } else {
        
            oldMessage = load(message.getId());
            if ( oldMessage.equals(message) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, message.getId());
        
            executeUpdate("update cw_bpm_messages set "
                          + "package_name = ?, upper_package_name = ?, name = ?, upper_name = ? "
                          + "where id = ?",
                          message.getPackageName(), ( message.getPackageName() != null ? message.getPackageName().toUpperCase() : null ), message.getName(), ( message.getName() != null ? message.getName().toUpperCase() : null ), message.getId() );
        
        }
        
        saveAudit(transactionAction, oldMessage, message);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        MessageObject oldMessageObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldMessageObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_bpm_messages where id = ?", id);
        
    }
}
