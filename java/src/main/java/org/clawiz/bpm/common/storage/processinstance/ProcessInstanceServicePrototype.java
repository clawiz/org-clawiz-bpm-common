package org.clawiz.bpm.common.storage.processinstance;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ProcessInstanceServicePrototype extends AbstractTypeService<ProcessInstanceObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.bpm.common.storage", "ProcessInstance");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessInstance"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      processInstance Checked object
    */
    public void check(ProcessInstanceObject processInstance) {
        
        if ( processInstance == null ) {
            throwException("Cannot check null ?", new Object[]{"ProcessInstanceObject"});
        }
        
        processInstance.fillDefaults();
        
        
    }
    
    /**
    * Create new ProcessInstanceObject instance and fill default values
    * 
    * @return     Created object
    */
    public ProcessInstanceObject create() {
        
        ProcessInstanceObject processInstance = new ProcessInstanceObject();
        processInstance.setService((ProcessInstanceService) this);
        
        processInstance.fillDefaults();
        
        return processInstance;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ProcessInstanceObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select process_id, object_id, active from cw_bpm_process_instances where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"ProcessInstance", id});
        }
        
        ProcessInstanceObject result = new ProcessInstanceObject();
        
        result.setService((ProcessInstanceService) this);
        result.setId(id);
        result.setProcessId(statement.getBigDecimal(1));
        result.setObjectId(statement.getBigDecimal(2));
        result.setActive(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ProcessInstanceObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ProcessInstanceList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ProcessInstanceObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ProcessInstanceList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ProcessInstanceList result = new ProcessInstanceList();
        
        
        Statement statement = executeQuery("select id, process_id, object_id, active from cw_bpm_process_instances"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ProcessInstanceObject object = new ProcessInstanceObject();
        
            object.setService((ProcessInstanceService) this);
            object.setId(statement.getBigDecimal(1));
            object.setProcessId(statement.getBigDecimal(2));
            object.setObjectId(statement.getBigDecimal(3));
            object.setActive(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ProcessObject' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToProcessObject(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select process_id, object_id from cw_bpm_process_instances where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ProcessInstanceObject object = new ProcessInstanceObject();
        object.setService((ProcessInstanceService)this);
        object.setId(id);
        object.setProcessId(statement.getBigDecimal(1));
        object.setObjectId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toProcessObject();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToProcessObject(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      processInstanceObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ProcessInstanceObject processInstanceObject) {
        return processInstanceObject.toProcessObject();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ProcessInstanceObject oldProcessInstanceObject, ProcessInstanceObject newProcessInstanceObject) {
        
        ProcessInstanceObject o = oldProcessInstanceObject != null ? oldProcessInstanceObject : new ProcessInstanceObject();
        ProcessInstanceObject n = newProcessInstanceObject != null ? newProcessInstanceObject : new ProcessInstanceObject();
        
        
        executeUpdate("insert into a_cw_bpm_process_instances (scn, action_type, id , o_process_id, o_object_id, o_active, n_process_id, n_object_id, n_active) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getProcessId(), o.getObjectId(), o.getActive(), n.getProcessId(), n.getObjectId(), n.getActive());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      processInstance Saved object
    */
    public void save(ProcessInstanceObject processInstance) {
        
        if ( processInstance == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ProcessInstanceObject"});
        }
        
        TransactionAction transactionAction;
        ProcessInstanceObject oldProcessInstance;
        if ( processInstance.getService() == null ) {
            processInstance.setService((ProcessInstanceService)this);
        }
        
        check(processInstance);
        
        if ( processInstance.getId() == null ) {
        
            processInstance.setId(getObjectService().createObject(getTypeId()));
        
            oldProcessInstance = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, processInstance.getId());
        
            executeUpdate("insert into cw_bpm_process_instances" 
                          + "( id, process_id, object_id, active ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          processInstance.getId(), processInstance.getProcessId(), processInstance.getObjectId(), processInstance.getActive() );
        
            if ( processInstance.getProcessId() != null ) { getObjectService().setLink(processInstance.getProcessId(), processInstance.getId()); }
            if ( processInstance.getObjectId() != null ) { getObjectService().setLink(processInstance.getObjectId(), processInstance.getId()); }
        
        } else {
        
            oldProcessInstance = load(processInstance.getId());
            if ( oldProcessInstance.equals(processInstance) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, processInstance.getId());
        
            executeUpdate("update cw_bpm_process_instances set "
                          + "process_id = ?, object_id = ?, active = ? "
                          + "where id = ?",
                          processInstance.getProcessId(), processInstance.getObjectId(), processInstance.getActive(), processInstance.getId() );
        
            getObjectService().changeLinkParent(oldProcessInstance.getProcessId(), processInstance.getProcessId(), processInstance.getId());
            getObjectService().changeLinkParent(oldProcessInstance.getObjectId(), processInstance.getObjectId(), processInstance.getId());
        
        }
        
        saveAudit(transactionAction, oldProcessInstance, processInstance);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ProcessInstanceObject oldProcessInstanceObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldProcessInstanceObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldProcessInstanceObject.getProcessId() != null) { getObjectService().deleteLink(oldProcessInstanceObject.getProcessId(), id); }
        if (oldProcessInstanceObject.getObjectId() != null) { getObjectService().deleteLink(oldProcessInstanceObject.getObjectId(), id); }
        
        executeUpdate("delete from cw_bpm_process_instances where id = ?", id);
        
    }
}
