/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.helper;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.ProcessList;
import org.clawiz.bpm.common.helper.ProcessHelper;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.StringUtils;

import java.util.HashMap;

public class ProcessGeneratorHelper extends Service {

    ProcessHelper processHelper;

    public ProcessList getProcessesByRootType(Type type) {
        return processHelper.getProcessesByRootType(type);
    }

    HashMap<Process, String> processClassesCommonPrefixes = new HashMap<>();
    public String getProcessClassCommonPrefix(Process process) {
        String name = processClassesCommonPrefixes.get(process);
        if ( name != null ) {
            return name;
        }

        name = process.getPackageName()
               + "." + GeneratorUtils.toJavaName(process.getName()).toLowerCase()
               + "." + StringUtils.toUpperFirstChar(GeneratorUtils.toJavaName(process.getName()));

        processClassesCommonPrefixes.put(process, name);
        return name;
    }

    HashMap<Process, String>  processServiceVariableNames = new HashMap<>();
    public String getProcessServiceVariableName(Process process) {
        String name = processServiceVariableNames.get(process);
        if ( name != null ) {
            return name;
        }

        name = StringUtils.toLowerFirstChar(process.getJavaVariableName()) + "Service";
        processServiceVariableNames.put(process, name);

        return name;

    }

    public void clearCaches() {
        processClassesCommonPrefixes.clear();
        processServiceVariableNames.clear();
    }

}
