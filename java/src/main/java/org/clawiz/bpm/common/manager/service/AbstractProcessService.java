/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.manager.service;

import org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.manager.ProcessManager;
import org.clawiz.bpm.common.manager.instance.AbstractProcessInstance;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.service.Service;

import java.math.BigDecimal;

public abstract class AbstractProcessService<T extends AbstractObject, I extends AbstractProcessInstance> extends Service {

    public ProcessManager processManager;

    public T loadObject(BigDecimal objectId) {
        return null;
    }

    abstract public BigDecimal startProcess(BigDecimal objectId);

    public boolean isNodePassed(AbstractProcessInstance instance, AbstractProcessNode node) {
        try {
            return (Boolean) node.getIsPassedMethod().invoke(this, instance);
        } catch (Exception e) {
            throwException("Exception on execute ? method of ? : ?", node.getIsPassedMethod().getName(), this.getClass().getName(), e.getMessage(), e);
        }
        return false;
    }

    public boolean isFlowAllowed(AbstractProcessInstance instance, AbstractFlow flow) {
        try {
            AbstractProcessNode node = instance.getNodes().get(flow.getSourceNode().getId());
            if ( node == null || ! isNodePassed(instance, node) ) {
                return false;
            }
            return (Boolean) flow.getIsAllowedMethod().invoke(this, instance);
        } catch (Exception e) {
            throwException("Exception on execute ? method of ? : ?", flow.getIsAllowedMethod().getName(), this.getClass().getName(), e.getMessage(), e);
        }
        return false;
    }

    public void processNode(AbstractProcessInstance instance, AbstractProcessNode node) {

        try {
            node.getProcessMethod().invoke(this, instance);
        } catch (Exception e) {
            throwException("Exception on execute ? method of ? : ?", node.getProcessMethod().getName(), this.getClass().getName(), e.getMessage(), e);
        }

    }

}
