package org.clawiz.bpm.common.metadata.data.process.flow.condition.eval;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class JavaScriptEvalFlowConditionPrototype extends org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.AbstractEvalFlowCondition {
    
    public JavaScriptEvalFlowCondition withName(String value) {
        setName(value);
        return (JavaScriptEvalFlowCondition) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
