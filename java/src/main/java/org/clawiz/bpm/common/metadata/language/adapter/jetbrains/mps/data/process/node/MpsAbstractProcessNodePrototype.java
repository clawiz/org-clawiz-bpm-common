package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractProcessNodePrototype extends AbstractMpsNode {
    
    public String description;
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane lane;
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.MpsAbstractFlow> outgoingFlows = new ArrayList<>();
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane getLane() {
        return this.lane;
    }
    
    public void setLane(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane value) {
        this.lane = value;
    }
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.MpsAbstractFlow> getOutgoingFlows() {
        return this.outgoingFlows;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7874312983645128481";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractProcessNode";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "7874312983645128481", "org.clawiz.bpm.common.language.structure.AbstractProcessNode", ConceptPropertyType.PROPERTY, "7874312983646300908", "description"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "7874312983645128481", "org.clawiz.bpm.common.language.structure.AbstractProcessNode", ConceptPropertyType.REFERENCE, "7874312983646269916", "lane"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "7874312983645128481", "org.clawiz.bpm.common.language.structure.AbstractProcessNode", ConceptPropertyType.CHILD, "7874312983645128702", "outgoingFlows"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("7874312983645128481", "description", getDescription());
        for (AbstractMpsNode value : getOutgoingFlows() ) {
            addConceptNodeChild("7874312983645128481", "outgoingFlows", value);
        }
        addConceptNodeRef("7874312983645128481", "lane", getLane());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode structure = (org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode) node;
        
        structure.setDescription(getDescription());
        
        if ( getLane() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "lane", false, getLane());
        } else {
            structure.setLane(null);
        }
        
        structure.getOutgoingFlows().clear();
        for (org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.MpsAbstractFlow mpsNode : getOutgoingFlows() ) {
            if ( mpsNode != null ) {
                structure.getOutgoingFlows().add((org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow) mpsNode.toMetadataNode(structure, "outgoingFlows"));
            } else {
                structure.getOutgoingFlows().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode structure = (org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode) node;
        
        setDescription(structure.getDescription());
        
        if ( structure.getLane() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "lane", false, structure.getLane());
        } else {
            setLane(null);
        }
        
        getOutgoingFlows().clear();
        for (org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow metadataNode : structure.getOutgoingFlows() ) {
            if ( metadataNode != null ) {
                getOutgoingFlows().add(loadChildMetadataNode(metadataNode));
            } else {
                getOutgoingFlows().add(null);
            }
        }
        
    }
}
