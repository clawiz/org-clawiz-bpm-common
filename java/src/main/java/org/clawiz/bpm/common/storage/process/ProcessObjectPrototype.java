package org.clawiz.bpm.common.storage.process;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ProcessObjectPrototype extends AbstractObject {
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * rootType
    */
    private java.math.BigDecimal rootTypeId;
    
    public ProcessService service;
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Process.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     Process object
    */
    public ProcessObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Process.Name", "128");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     Process object
    */
    public ProcessObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     rootType
    */
    public java.math.BigDecimal getRootTypeId() {
        return this.rootTypeId;
    }
    
    /**
    * Set 'rootType' value
    * 
    * @param      rootTypeId rootType
    */
    public void setRootTypeId(java.math.BigDecimal rootTypeId) {
         this.rootTypeId = rootTypeId;
    }
    
    /**
    * Set 'rootType' value and return this object
    * 
    * @param      rootTypeId rootType
    * @return     Process object
    */
    public ProcessObjectPrototype withRootTypeId(java.math.BigDecimal rootTypeId) {
        setRootTypeId(rootTypeId);
        return this;
    }
    
    public ProcessService getService() {
        return this.service;
    }
    
    public void setService(ProcessService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ProcessObjectPrototype  target) {
        target.setPackageName(getPackageName());
        target.setName(getName());
        target.setRootTypeId(getRootTypeId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ProcessObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((ProcessObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ProcessObjectPrototype object) {
        return 
               isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getRootTypeId(), object.getRootTypeId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getRootTypeId() != null ? getRootTypeId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'PackageName' fields
    * 
    * @return     Concatenated string values of key 'toPackageName' fields
    */
    public String toPackageName() {
        return getPackageName() + "," + getName();
    }
    
    /**
    * Prepare concatenated string values of key 'RootType' fields
    * 
    * @return     Concatenated string values of key 'toRootType' fields
    */
    public String toRootType() {
        return service.getObjectService().idToString(getRootTypeId());
    }
}
