package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.user;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsUserTaskActivityPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.user.MpsAbstractUserTaskActivity {
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7874312983645220795";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.UserTaskActivity";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.activity.task.user.UserTaskActivity.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.user.UserTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.user.UserTaskActivity) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.user.UserTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.user.UserTaskActivity) node;
        
    }
}
