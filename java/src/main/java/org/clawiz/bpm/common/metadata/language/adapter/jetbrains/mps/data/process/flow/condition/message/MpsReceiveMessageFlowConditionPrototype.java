package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.message;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsReceiveMessageFlowConditionPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition {
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsAbstractMessage message;
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsAbstractMessage getMessage() {
        return this.message;
    }
    
    public void setMessage(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsAbstractMessage value) {
        this.message = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1512176054398689236";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.ReceiveMessageFlowCondition";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "1512176054398689236", "org.clawiz.bpm.common.language.structure.ReceiveMessageFlowCondition", ConceptPropertyType.REFERENCE, "1512176054398689237", "message"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("1512176054398689236", "message", getMessage());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.flow.condition.message.ReceiveMessageFlowCondition.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.flow.condition.message.ReceiveMessageFlowCondition structure = (org.clawiz.bpm.common.metadata.data.process.flow.condition.message.ReceiveMessageFlowCondition) node;
        
        if ( getMessage() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "message", false, getMessage());
        } else {
            structure.setMessage(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.flow.condition.message.ReceiveMessageFlowCondition structure = (org.clawiz.bpm.common.metadata.data.process.flow.condition.message.ReceiveMessageFlowCondition) node;
        
        if ( structure.getMessage() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "message", false, structure.getMessage());
        } else {
            setMessage(null);
        }
        
    }
}
