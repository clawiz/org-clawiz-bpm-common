/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.manager.instance;


import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.node.ProcessNodeList;
import org.clawiz.bpm.common.manager.service.AbstractProcessService;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;

import java.math.BigDecimal;

public abstract class AbstractProcessInstance <T extends AbstractObject> {

    BigDecimal                   id;

    Session                      session;
    AbstractProcessService       service;
    BigDecimal                   objectId;
    AbstractObject               object;

    Process                      process;
    ProcessNodeList              nodes = new ProcessNodeList();

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getObjectId() {
        return objectId;
    }

    public void setObjectId(BigDecimal objectId) {
        this.object   = null;
        this.objectId = objectId;
    }

    public T getObject() {
        if ( object == null ) {
            object = getService().loadObject(objectId);
        }
        return (T) object;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public AbstractProcessService getService() {
        if ( service == null ) {
            service = session.getService(process.getServiceClass());
        }
        return service;
    }

    public ProcessNodeList getNodes() {
        return nodes;
    }

    abstract public ProcessInstanceParameterList toParameterList();

    abstract public void fromParameterList(ProcessInstanceParameterList parameters);

    public void clearCaches() {
        this.object = null;
    }

}
