package org.clawiz.bpm.common.storage.processinstance;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ProcessInstanceModelPrototype extends TypeModel {
    
    private static TypeField PROCESS_ID_FIELD;
    
    private static TypeField OBJECT_ID_FIELD;
    
    private static TypeField ACTIVE_FIELD;
    
    private static Type type;
    
    private ProcessInstanceService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ProcessInstanceService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ProcessInstanceService.class).getType();
        
        
        PROCESS_ID_FIELD = type.getFields().get("Process");
        if ( PROCESS_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessInstance.Process"}); }
        
        OBJECT_ID_FIELD = type.getFields().get("Object");
        if ( OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessInstance.Object"}); }
        
        ACTIVE_FIELD = type.getFields().get("active");
        if ( ACTIVE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessInstance.active"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _processId;
    
    public TypeFieldModel processId() {
        
        if ( _processId != null ) {
            return _processId;
        }
        
        _processId = new TypeFieldModel(this, PROCESS_ID_FIELD);
        return _processId;
        
    }
    
    private TypeFieldModel _objectId;
    
    public TypeFieldModel objectId() {
        
        if ( _objectId != null ) {
            return _objectId;
        }
        
        _objectId = new TypeFieldModel(this, OBJECT_ID_FIELD);
        return _objectId;
        
    }
    
    private TypeFieldModel _active;
    
    public TypeFieldModel active() {
        
        if ( _active != null ) {
            return _active;
        }
        
        _active = new TypeFieldModel(this, ACTIVE_FIELD);
        return _active;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PROCESS" : return processId();
        case "OBJECT" : return objectId();
        case "ACTIVE" : return active();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
