/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.type.component;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.generator.helper.ProcessGeneratorHelper;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.extension.ServiceExtensionMethod;
import org.clawiz.core.common.system.type.generator.component.AbstractTypeGeneratorJavaClassComponentExtension;

import static org.clawiz.core.common.system.type.generator.component.service.TypeServiceClassPrototypeComponent.AFTER_PROCESS_EXTENSION_NAME;

public class BpmTypeServiceClassPrototypeComponentExtension extends AbstractTypeGeneratorJavaClassComponentExtension {

    ProcessGeneratorHelper processGeneratorHelper;

    @ServiceExtensionMethod(name = AFTER_PROCESS_EXTENSION_NAME)
    public void afterProcess() {

        for (Process process : processGeneratorHelper.getProcessesByRootType(getGenerator().getType())) {

            getComponent().addImport(NotInitializeService.class);
            getComponent().addImport(process.getServicePackageName() + "." + process.getServiceClassName());

            String serviceVariableName = "_" + processGeneratorHelper.getProcessServiceVariableName(process);
            getComponent().addVariable(process.getServiceClassName(), serviceVariableName)
                    .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE)
                    .addAnnotation(NotInitializeService.class);


            JavaMethodElement method = getComponent().addMethod(process.getServiceClassName(), "get" + process.getServiceClassName());
            method.addText("if ( " + serviceVariableName + " == null ) {");
            method.addText("    " + serviceVariableName +  " = getService(" + process.getServiceClassName() + ".class);");
            method.addText("}");
            method.addText("return " +  serviceVariableName + ";");


        }
    }

}
