package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsProcessPrototype extends AbstractMpsNode {
    
    public String description;
    
    public Boolean startOnNew;
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.property.MpsAbstractProcessProperty> properties = new ArrayList<>();
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode> nodes = new ArrayList<>();
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane> lanes = new ArrayList<>();
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType rootType;
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.pool.MpsPool pool;
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public Boolean isStartOnNew() {
        return this.startOnNew;
    }
    
    public void setStartOnNew(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.startOnNew = null;
        }
        this.startOnNew = StringUtils.toBoolean(value);
    }
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.property.MpsAbstractProcessProperty> getProperties() {
        return this.properties;
    }
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode> getNodes() {
        return this.nodes;
    }
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane> getLanes() {
        return this.lanes;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getRootType() {
        return this.rootType;
    }
    
    public void setRootType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.rootType = value;
    }
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.pool.MpsPool getPool() {
        return this.pool;
    }
    
    public void setPool(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.pool.MpsPool value) {
        this.pool = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2359901387388748492";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.Process";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.PROPERTY, "3183694419911792178", "description"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.PROPERTY, "7874312983645669973", "startOnNew"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.CHILD, "3183694419911888998", "properties"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.CHILD, "7874312983645128492", "nodes"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.CHILD, "7874312983646269668", "lanes"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.REFERENCE, "645131847789189685", "rootType"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "2359901387388748492", "org.clawiz.bpm.common.language.structure.Process", ConceptPropertyType.REFERENCE, "7874312983646286169", "pool"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("2359901387388748492", "description", getDescription());
        addConceptNodeProperty("2359901387388748492", "startOnNew", isStartOnNew() != null ? isStartOnNew().toString() : null);
        for (AbstractMpsNode value : getProperties() ) {
            addConceptNodeChild("2359901387388748492", "properties", value);
        }
        for (AbstractMpsNode value : getNodes() ) {
            addConceptNodeChild("2359901387388748492", "nodes", value);
        }
        for (AbstractMpsNode value : getLanes() ) {
            addConceptNodeChild("2359901387388748492", "lanes", value);
        }
        addConceptNodeRef("2359901387388748492", "rootType", getRootType());
        addConceptNodeRef("2359901387388748492", "pool", getPool());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.Process.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.Process structure = (org.clawiz.bpm.common.metadata.data.process.Process) node;
        
        structure.setDescription(getDescription());
        
        structure.setStartOnNew(isStartOnNew());
        
        structure.getProperties().clear();
        for (org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.property.MpsAbstractProcessProperty mpsNode : getProperties() ) {
            if ( mpsNode != null ) {
                structure.getProperties().add((org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty) mpsNode.toMetadataNode(structure, "properties"));
            } else {
                structure.getProperties().add(null);
            }
        }
        
        structure.getNodes().clear();
        for (org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode mpsNode : getNodes() ) {
            if ( mpsNode != null ) {
                structure.getNodes().add((org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode) mpsNode.toMetadataNode(structure, "nodes"));
            } else {
                structure.getNodes().add(null);
            }
        }
        
        structure.getLanes().clear();
        for (org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.swimlane.lane.MpsLane mpsNode : getLanes() ) {
            if ( mpsNode != null ) {
                structure.getLanes().add((org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane) mpsNode.toMetadataNode(structure, "lanes"));
            } else {
                structure.getLanes().add(null);
            }
        }
        
        if ( getRootType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "rootType", false, getRootType());
        } else {
            structure.setRootType(null);
        }
        
        if ( getPool() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "pool", false, getPool());
        } else {
            structure.setPool(null);
        }
        
    }
    
    public void fillForeignKeys() {
        addForeignKey(getRootType());
        addForeignKey(getPool());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.Process structure = (org.clawiz.bpm.common.metadata.data.process.Process) node;
        
        setDescription(structure.getDescription());
        
        setStartOnNew(structure.isStartOnNew() ? "true" : "false");
        
        getProperties().clear();
        for (org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty metadataNode : structure.getProperties() ) {
            if ( metadataNode != null ) {
                getProperties().add(loadChildMetadataNode(metadataNode));
            } else {
                getProperties().add(null);
            }
        }
        
        getNodes().clear();
        for (org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode metadataNode : structure.getNodes() ) {
            if ( metadataNode != null ) {
                getNodes().add(loadChildMetadataNode(metadataNode));
            } else {
                getNodes().add(null);
            }
        }
        
        getLanes().clear();
        for (org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane metadataNode : structure.getLanes() ) {
            if ( metadataNode != null ) {
                getLanes().add(loadChildMetadataNode(metadataNode));
            } else {
                getLanes().add(null);
            }
        }
        
        if ( structure.getRootType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "rootType", false, structure.getRootType());
        } else {
            setRootType(null);
        }
        
        if ( structure.getPool() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "pool", false, structure.getPool());
        } else {
            setPool(null);
        }
        
    }
}
