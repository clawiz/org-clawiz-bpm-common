/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.ui.helper.generator.form;

import org.clawiz.bpm.common.generator.helper.ProcessGeneratorHelper;
import org.clawiz.bpm.common.ui.data.common.layout.button.ProcessFlowButton;
import org.clawiz.bpm.common.ui.data.common.layout.text.ProcessStateText;
import org.clawiz.core.common.system.service.extension.ServiceExtension;
import org.clawiz.core.common.system.service.extension.ServiceExtensionMethod;
import org.clawiz.ui.common.metadata.data.layout.container.collection.IteratorCollection;
import org.clawiz.ui.common.metadata.data.layout.container.collection.direction.CollectionDirection;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorHelper;

public class BpmFormGeneratorHelperExtension extends ServiceExtension {

    ProcessGeneratorHelper processGeneratorHelper;


    Form form;

    protected void addFlowButtons() {

        IteratorCollection instances = new IteratorCollection();

        form.getTopToolbar().getLeft().add(instances);
        instances.setIteratorName("processInstances.instances");
        instances.setVariableName("instance");

        IteratorCollection flows = new IteratorCollection();
        instances.add(flows);
        flows.setIteratorName("instance.allowedFlows");
        flows.setVariableName("flow");
        flows.setDirection(CollectionDirection.HORIZONTAL);

        ProcessFlowButton button = new ProcessFlowButton();
        flows.add(button);

    }

    protected void addNodeTexts() {

        IteratorCollection instances = new IteratorCollection();
        form.getTopToolbar().getRight().add(instances);
        instances.setIteratorName("processInstances.instances");
        instances.setVariableName("instance");

        IteratorCollection nodes = new IteratorCollection();
        instances.add(nodes);
        nodes.setIteratorName("instance.nodes");
        nodes.setVariableName("node");

        ProcessStateText text = new ProcessStateText();
        nodes.add(text);
    }

    @ServiceExtensionMethod(name = FormGeneratorHelper.AFTER_PREPARE_FORM_EXTENSION_NAME)
    public void afterPrepareForm(Form form) {

        this.form = form;

        if ( processGeneratorHelper.getProcessesByRootType(form.getRootType()).size() == 0 ) {
            return;
        }

        addFlowButtons();
        addNodeTexts();

    }

}
