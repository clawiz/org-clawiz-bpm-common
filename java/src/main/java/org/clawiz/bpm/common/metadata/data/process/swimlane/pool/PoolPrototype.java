package org.clawiz.bpm.common.metadata.data.process.swimlane.pool;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class PoolPrototype extends org.clawiz.bpm.common.metadata.data.process.swimlane.pool.AbstractPool {
    
    public Pool withName(String value) {
        setName(value);
        return (Pool) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
