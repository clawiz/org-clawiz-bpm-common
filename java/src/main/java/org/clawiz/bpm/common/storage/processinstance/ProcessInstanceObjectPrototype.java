package org.clawiz.bpm.common.storage.processinstance;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ProcessInstanceObjectPrototype extends AbstractObject {
    
    /**
    * Process
    */
    private java.math.BigDecimal processId;
    
    /**
    * Object
    */
    private java.math.BigDecimal objectId;
    
    /**
    * active
    */
    private java.lang.String active;
    
    public ProcessInstanceService service;
    
    /**
    * 
    * @return     Process
    */
    public java.math.BigDecimal getProcessId() {
        return this.processId;
    }
    
    /**
    * Set 'Process' value
    * 
    * @param      processId Process
    */
    public void setProcessId(java.math.BigDecimal processId) {
         this.processId = processId;
    }
    
    /**
    * Set 'Process' value and return this object
    * 
    * @param      processId Process
    * @return     ProcessInstance object
    */
    public ProcessInstanceObjectPrototype withProcessId(java.math.BigDecimal processId) {
        setProcessId(processId);
        return this;
    }
    
    /**
    * 
    * @return     Object
    */
    public java.math.BigDecimal getObjectId() {
        return this.objectId;
    }
    
    /**
    * Set 'Object' value
    * 
    * @param      objectId Object
    */
    public void setObjectId(java.math.BigDecimal objectId) {
         this.objectId = objectId;
    }
    
    /**
    * Set 'Object' value and return this object
    * 
    * @param      objectId Object
    * @return     ProcessInstance object
    */
    public ProcessInstanceObjectPrototype withObjectId(java.math.BigDecimal objectId) {
        setObjectId(objectId);
        return this;
    }
    
    /**
    * 
    * @return     active
    */
    public java.lang.String getActive() {
        return this.active;
    }
    
    /**
    * Set 'active' value
    * 
    * @param      active active
    */
    public void setActive(java.lang.String active) {
        
        if ( active != null && active.length() > 1) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "ProcessInstance.active", "1");
        }
        
         this.active = active;
    }
    
    /**
    * Set 'active' value and return this object
    * 
    * @param      active active
    * @return     ProcessInstance object
    */
    public ProcessInstanceObjectPrototype withActive(java.lang.String active) {
        setActive(active);
        return this;
    }
    
    public ProcessInstanceService getService() {
        return this.service;
    }
    
    public void setService(ProcessInstanceService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ProcessInstanceObjectPrototype  target) {
        target.setProcessId(getProcessId());
        target.setObjectId(getObjectId());
        target.setActive(getActive());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ProcessInstanceObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((ProcessInstanceObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ProcessInstanceObjectPrototype object) {
        return 
               isObjectsEquals(this.getProcessId(), object.getProcessId() ) 
            && isObjectsEquals(this.getObjectId(), object.getObjectId() ) 
            && isObjectsEquals(this.getActive(), object.getActive() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getProcessId() != null ? getProcessId().hashCode() : 0);
        result = result * 31 + (getObjectId() != null ? getObjectId().hashCode() : 0);
        result = result * 31 + (getActive() != null ? getActive().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'ProcessObject' fields
    * 
    * @return     Concatenated string values of key 'toProcessObject' fields
    */
    public String toProcessObject() {
        return service.getObjectService().idToString(getProcessId()) + "," + service.getObjectService().idToString(getObjectId());
    }
}
