package org.clawiz.bpm.common.metadata.data.process.node.event.start;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractStartEventPrototype extends org.clawiz.bpm.common.metadata.data.process.node.event.AbstractEvent {
    
    public AbstractStartEvent withName(String value) {
        setName(value);
        return (AbstractStartEvent) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
