/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common;

import org.clawiz.bpm.common.generator.type.component.BpmTypeServiceClassPrototypeComponentExtension;
import org.clawiz.bpm.common.generator.type.component.element.BpmTypeServiceSaveMethodElementExtension;
import org.clawiz.bpm.common.ui.generator.client.ts.ui.form.component.BpmTypeScriptFormComponentPrototypeComponentExtension;
import org.clawiz.bpm.common.ui.generator.client.ts.ui.form.component.BpmTypeScriptFormModelPrototypeComponentExtension;
import org.clawiz.bpm.common.ui.generator.server.java.view.form.api.component.BpmFormApiModelPrototypeComponentExtension;
import org.clawiz.bpm.common.ui.generator.server.java.view.form.api.component.BpmFormApiRequestContextPrototypeComponentExtension;
import org.clawiz.bpm.common.ui.generator.server.java.view.form.api.component.BpmFormApiResponseContextPrototypeComponentExtension;
import org.clawiz.bpm.common.ui.generator.server.java.view.form.api.component.BpmFormApiServletPrototypeComponentExtension;
import org.clawiz.bpm.common.ui.generator.server.java.view.form.api.component.element.*;
import org.clawiz.bpm.common.ui.helper.generator.form.BpmFormGeneratorHelperExtension;
import org.clawiz.core.common.system.module.Module;
import org.clawiz.core.common.system.module.ModuleDefinition;
import org.clawiz.core.common.system.service.extension.ServiceExtensionConfigList;
import org.clawiz.core.common.system.type.generator.component.service.TypeServiceClassPrototypeComponent;
import org.clawiz.core.common.system.type.generator.component.service.element.save.TypeServiceSaveMethodElement;
import org.clawiz.metadata.jetbrains.mps.JetbrainsMpsMetadataModule;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormModelPrototypeComponent;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorHelper;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiModelPrototypeComponent;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiRequestContextPrototypeComponent;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiResponseContextPrototypeComponent;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.FormApiServletPrototypeComponent;
import org.clawiz.ui.common.generator.server.java.view.form.api.component.element.*;

public class BpmCommonModule extends Module {

    @Override
    protected void fillDefinition(ModuleDefinition definition) {
        super.fillDefinition(definition);

        definition.addRequire(JetbrainsMpsMetadataModule.class);

    }

    @Override
    public ServiceExtensionConfigList getServiceExtensionConfigList() {

        return super.getServiceExtensionConfigList()
                .add(TypeServiceSaveMethodElement.class,                       BpmTypeServiceSaveMethodElementExtension.class)
                .add(TypeServiceClassPrototypeComponent.class,                 BpmTypeServiceClassPrototypeComponentExtension.class)
                .add(FormApiModelPrototypeComponent.class,                     BpmFormApiModelPrototypeComponentExtension.class)
                .add(FormApiResponseContextPrototypeComponent.class,           BpmFormApiResponseContextPrototypeComponentExtension.class)
                .add(FormApiRequestContextPrototypeComponent.class,            BpmFormApiRequestContextPrototypeComponentExtension.class)
                .add(FormApiServletPrototypeComponent.class,                   BpmFormApiServletPrototypeComponentExtension.class)
                .add(FormApiServletCreateRequestMethodElement.class,           BpmFormApiServletCreateRequestMethodElementExtension.class)
                .add(FormApiServletPrepareQueryMethodElement.class,            BpmFormApiServletPrepareQueryMethodElementExtension.class)
                .add(FormApiServletModelToStringMapMethodElement.class,        BpmFormApiServletModelToStringMapMethodElementExtension.class)
                .add(FormApiServletPrepareModelMethodElement.class,            BpmFormApiServletPrepareModelMethodElementExtension.class)
                .add(FormApiServletPrepareResponseMethodElement.class,         BpmFormApiServletPrepareResponseMethodElementExtension.class)
                .add(FormApiServletLoadMethodElement.class,                    BpmFormApiServletLoadMethodElementExtension.class)
                .add(FormApiServletProcessDoPostMethodElement.class,           BpmFormApiServletProcessDoPostMethodElementExtension.class)
                .add(TypeScriptFormModelPrototypeComponent.class,              BpmTypeScriptFormModelPrototypeComponentExtension.class)
                .add(TypeScriptFormComponentPrototypeComponent.class,          BpmTypeScriptFormComponentPrototypeComponentExtension.class)
                .add(FormGeneratorHelper.class,                                BpmFormGeneratorHelperExtension.class)
                ;

    }
}
