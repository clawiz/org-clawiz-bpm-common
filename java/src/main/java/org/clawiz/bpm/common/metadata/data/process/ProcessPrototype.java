package org.clawiz.bpm.common.metadata.data.process;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.Boolean;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ProcessPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    @ExchangeAttribute
    private java.lang.Boolean startOnNew;
    
    @ExchangeElement
    private org.clawiz.bpm.common.metadata.data.process.property.ProcessPropertyList properties = new org.clawiz.bpm.common.metadata.data.process.property.ProcessPropertyList();
    
    @ExchangeElement
    private org.clawiz.bpm.common.metadata.data.process.node.ProcessNodeList nodes = new org.clawiz.bpm.common.metadata.data.process.node.ProcessNodeList();
    
    @ExchangeElement
    private org.clawiz.bpm.common.metadata.data.process.swimlane.lane.LaneList lanes = new org.clawiz.bpm.common.metadata.data.process.swimlane.lane.LaneList();
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type rootType;
    
    @ExchangeReference
    private org.clawiz.bpm.common.metadata.data.process.swimlane.pool.Pool pool;
    
    public Process withName(String value) {
        setName(value);
        return (Process) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public Process withDescription(String value) {
        setDescription(value);
        return (Process) this;
    }
    
    public java.lang.Boolean isStartOnNew() {
        return this.startOnNew;
    }
    
    public void setStartOnNew(java.lang.Boolean value) {
        this.startOnNew = value;
    }
    
    public Process withStartOnNew(java.lang.Boolean value) {
        setStartOnNew(value);
        return (Process) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.property.ProcessPropertyList getProperties() {
        return this.properties;
    }
    
    public Process withPropertie(org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty value) {
        getProperties().add(value);
        return (Process) this;
    }
    
    public <T extends org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty> T createPropertie(Class<T> nodeClass) {
        org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty value = createChildNode(nodeClass, "properties");
        getProperties().add(value);
        return (T) value;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty createPropertie() {
        return createPropertie(org.clawiz.bpm.common.metadata.data.process.property.AbstractProcessProperty.class);
    }
    
    public org.clawiz.bpm.common.metadata.data.process.node.ProcessNodeList getNodes() {
        return this.nodes;
    }
    
    public Process withNode(org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode value) {
        getNodes().add(value);
        return (Process) this;
    }
    
    public <T extends org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode> T createNode(Class<T> nodeClass) {
        org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode value = createChildNode(nodeClass, "nodes");
        getNodes().add(value);
        return (T) value;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode createNode() {
        return createNode(org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode.class);
    }
    
    public org.clawiz.bpm.common.metadata.data.process.swimlane.lane.LaneList getLanes() {
        return this.lanes;
    }
    
    public Process withLane(org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane value) {
        getLanes().add(value);
        return (Process) this;
    }
    
    public <T extends org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane> T createLane(Class<T> nodeClass) {
        org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane value = createChildNode(nodeClass, "lanes");
        getLanes().add(value);
        return (T) value;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane createLane() {
        return createLane(org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getRootType() {
        return this.rootType;
    }
    
    public void setRootType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.rootType = value;
    }
    
    public Process withRootType(org.clawiz.core.common.metadata.data.type.Type value) {
        setRootType(value);
        return (Process) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.swimlane.pool.Pool getPool() {
        return this.pool;
    }
    
    public void setPool(org.clawiz.bpm.common.metadata.data.process.swimlane.pool.Pool value) {
        this.pool = value;
    }
    
    public Process withPool(org.clawiz.bpm.common.metadata.data.process.swimlane.pool.Pool value) {
        setPool(value);
        return (Process) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getProperties()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getNodes()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getLanes()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getRootType() != null ) { 
            getRootType().prepare(session);
        }
        if ( getPool() != null ) { 
            getPool().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getProperties()) {
            references.add(node);
        }
        
        for (MetadataNode node : getNodes()) {
            references.add(node);
        }
        
        for (MetadataNode node : getLanes()) {
            references.add(node);
        }
        
        references.add(getRootType());
        
        references.add(getPool());
        
    }
}
