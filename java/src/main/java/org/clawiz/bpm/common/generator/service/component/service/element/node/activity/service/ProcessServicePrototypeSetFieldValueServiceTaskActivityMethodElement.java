/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service.component.service.element.node.activity.service;

import org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeNodeProcessMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeString;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.utils.StringUtils;

public class ProcessServicePrototypeSetFieldValueServiceTaskActivityMethodElement extends AbstractProcessServicePrototypeNodeProcessMethodElement {

    @Override
    public void process() {
        super.process();

        SetFieldValueServiceTaskActivity activity = (SetFieldValueServiceTaskActivity) getNode();

        TypeField         typeField = activity.getTypeField();
        AbstractValueType valueType = typeField.getValueType();

        if ( activity.getValue() == null ) {
            addText("instance.getObject()." + typeField.getSetMethodName() + "(null);");
        } else {
            String setter = activity.getValue();
            if ( valueType instanceof ValueTypeEnumeration ) {
                ValueTypeEnumeration enumeration = (ValueTypeEnumeration) valueType;
                setter = typeField.getType().getServicePackageName() + "." + StringUtils.toUpperFirstChar(typeField.getJavaClassName()) + "." + setter.toUpperCase();
            } else if ( valueType instanceof ValueTypeString ) {
                setter = "\"" + setter + "\"";
            } else {
                throwException("Field ? value type ? not supported in setFieldTaskActivity for process ?", typeField.getFullName()
                        , valueType.getClass().getSimpleName()
                        , getProcess().getFullName());
            }
            addText("instance.getObject()." + typeField.getSetMethodName() + "(" + setter + ");");
        }

        addText("");

    }
}
