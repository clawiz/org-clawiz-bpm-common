package org.clawiz.bpm.common.storage.processnode;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ProcessNodeObjectPrototype extends AbstractObject {
    
    /**
    * Process
    */
    private java.math.BigDecimal processId;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    public ProcessNodeService service;
    
    /**
    * 
    * @return     Process
    */
    public java.math.BigDecimal getProcessId() {
        return this.processId;
    }
    
    /**
    * Set 'Process' value
    * 
    * @param      processId Process
    */
    public void setProcessId(java.math.BigDecimal processId) {
         this.processId = processId;
    }
    
    /**
    * Set 'Process' value and return this object
    * 
    * @param      processId Process
    * @return     ProcessNode object
    */
    public ProcessNodeObjectPrototype withProcessId(java.math.BigDecimal processId) {
        setProcessId(processId);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "ProcessNode.Name", "128");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     ProcessNode object
    */
    public ProcessNodeObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    public ProcessNodeService getService() {
        return this.service;
    }
    
    public void setService(ProcessNodeService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ProcessNodeObjectPrototype  target) {
        target.setProcessId(getProcessId());
        target.setName(getName());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ProcessNodeObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((ProcessNodeObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ProcessNodeObjectPrototype object) {
        return 
               isObjectsEquals(this.getProcessId(), object.getProcessId() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getProcessId() != null ? getProcessId().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'ProcessName' fields
    * 
    * @return     Concatenated string values of key 'toProcessName' fields
    */
    public String toProcessName() {
        return service.getObjectService().idToString(getProcessId()) + "," + getName();
    }
}
