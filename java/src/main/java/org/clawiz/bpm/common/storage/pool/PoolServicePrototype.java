package org.clawiz.bpm.common.storage.pool;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class PoolServicePrototype extends AbstractTypeService<PoolObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.bpm.common.storage", "Pool");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Pool"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      pool Checked object
    */
    public void check(PoolObject pool) {
        
        if ( pool == null ) {
            throwException("Cannot check null ?", new Object[]{"PoolObject"});
        }
        
        pool.fillDefaults();
        
        
        if ( packageNameToId(pool.getPackageName(), pool.getName(), pool.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Pool", pool.toPackageName() });
        }
        
    }
    
    /**
    * Create new PoolObject instance and fill default values
    * 
    * @return     Created object
    */
    public PoolObject create() {
        
        PoolObject pool = new PoolObject();
        pool.setService((PoolService) this);
        
        pool.fillDefaults();
        
        return pool;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public PoolObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, name from cw_bpm_pools where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Pool", id});
        }
        
        PoolObject result = new PoolObject();
        
        result.setService((PoolService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setName(statement.getString(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of PoolObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public PoolList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of PoolObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public PoolList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        PoolList result = new PoolList();
        
        
        Statement statement = executeQuery("select id, package_name, name from cw_bpm_pools"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            PoolObject object = new PoolObject();
        
            object.setService((PoolService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setName(statement.getString(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'PackageName' fields
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name) {
        return packageNameToId(packageName, name, null);
    }
    
    /**
    * Find id of record by key 'PackageName' fields with id not equal skipId
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, BigDecimal skipId) {
        
        if ( packageName == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_bpm_pools where upper_package_name = ? and upper_name = ?", packageName.toUpperCase(), name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_bpm_pools where upper_package_name = ? and upper_name = ? and id != ?", packageName.toUpperCase(), name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'PackageName' fields or create new record with values set to given parameters
    * 
    * @param      packageName         PackageName
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = packageNameToId(packageName, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        PoolObject object = create();
        object.setPackageName(packageName);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'PackageName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToPackageName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select package_name, name from cw_bpm_pools where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        PoolObject object = new PoolObject();
        object.setService((PoolService)this);
        object.setId(id);
        object.setPackageName(statement.getString(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toPackageName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToPackageName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      poolObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(PoolObject poolObject) {
        return poolObject.toPackageName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, PoolObject oldPoolObject, PoolObject newPoolObject) {
        
        PoolObject o = oldPoolObject != null ? oldPoolObject : new PoolObject();
        PoolObject n = newPoolObject != null ? newPoolObject : new PoolObject();
        
        
        executeUpdate("insert into a_cw_bpm_pools (scn, action_type, id , o_package_name, o_upper_package_name, o_name, o_upper_name, n_package_name, n_upper_package_name, n_name, n_upper_name) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      pool Saved object
    */
    public void save(PoolObject pool) {
        
        if ( pool == null ) {
            throwException("Cannot save NULL ?", new Object[]{"PoolObject"});
        }
        
        TransactionAction transactionAction;
        PoolObject oldPool;
        if ( pool.getService() == null ) {
            pool.setService((PoolService)this);
        }
        
        check(pool);
        
        if ( pool.getId() == null ) {
        
            pool.setId(getObjectService().createObject(getTypeId()));
        
            oldPool = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, pool.getId());
        
            executeUpdate("insert into cw_bpm_pools" 
                          + "( id, package_name, upper_package_name, name, upper_name ) "
                          + "values"
                          + "(?, ?, ?, ?, ?)",
                          pool.getId(), pool.getPackageName(), ( pool.getPackageName() != null ? pool.getPackageName().toUpperCase() : null ), pool.getName(), ( pool.getName() != null ? pool.getName().toUpperCase() : null ) );
        
        } else {
        
            oldPool = load(pool.getId());
            if ( oldPool.equals(pool) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, pool.getId());
        
            executeUpdate("update cw_bpm_pools set "
                          + "package_name = ?, upper_package_name = ?, name = ?, upper_name = ? "
                          + "where id = ?",
                          pool.getPackageName(), ( pool.getPackageName() != null ? pool.getPackageName().toUpperCase() : null ), pool.getName(), ( pool.getName() != null ? pool.getName().toUpperCase() : null ), pool.getId() );
        
        }
        
        saveAudit(transactionAction, oldPool, pool);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        PoolObject oldPoolObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldPoolObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_bpm_pools where id = ?", id);
        
    }
}
