/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service.component.service.element;

import org.clawiz.bpm.common.generator.service.component.service.ProcessServicePrototypeComponent;

public class ProcessServicePrototypeInitMethodElement extends AbstractProcessServicePrototypeJavaMethodElement {

    @Override
    public void process() {
        super.process();

        addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");

        setName("init");

        addText("");
        addText("super.init();");
        addText("");
        addText("if ( _process == null ) {");
        addText("    ProcessService processService = getService(ProcessService.class);");
        addText("    BigDecimal processId = processService.fullNameToId(" + ProcessServicePrototypeComponent.getProcessNameStaticVariableName(getProcess()) + ");");
        addText("    if ( processId == null ) {");
        addText("        throwException(\"Process '?' not registered in database\", " + ProcessServicePrototypeComponent.getProcessNameStaticVariableName(getProcess()) + ");");
        addText("    }");
        addText("    _process   = processService.getProcess(processId);");
        addText("}");
        addText("");

    }
}
