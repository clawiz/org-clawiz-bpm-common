package org.clawiz.bpm.common.storage.processnode;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ProcessNodeModelPrototype extends TypeModel {
    
    private static TypeField PROCESS_ID_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static Type type;
    
    private ProcessNodeService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ProcessNodeService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ProcessNodeService.class).getType();
        
        
        PROCESS_ID_FIELD = type.getFields().get("Process");
        if ( PROCESS_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessNode.Process"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessNode.Name"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _processId;
    
    public TypeFieldModel processId() {
        
        if ( _processId != null ) {
            return _processId;
        }
        
        _processId = new TypeFieldModel(this, PROCESS_ID_FIELD);
        return _processId;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PROCESS" : return processId();
        case "NAME" : return name();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
