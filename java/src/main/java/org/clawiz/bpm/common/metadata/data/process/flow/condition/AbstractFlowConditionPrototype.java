package org.clawiz.bpm.common.metadata.data.process.flow.condition;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractFlowConditionPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractFlowCondition withName(String value) {
        setName(value);
        return (AbstractFlowCondition) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
