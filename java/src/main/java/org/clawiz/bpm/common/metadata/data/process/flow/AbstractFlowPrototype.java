package org.clawiz.bpm.common.metadata.data.process.flow;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractFlowPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String caption;
    
    @ExchangeElement
    private org.clawiz.bpm.common.metadata.data.process.flow.condition.FlowConditionList conditions = new org.clawiz.bpm.common.metadata.data.process.flow.condition.FlowConditionList();
    
    @ExchangeReference
    private org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode targetNode;
    
    public AbstractFlow withName(String value) {
        setName(value);
        return (AbstractFlow) this;
    }
    
    public String getCaption() {
        return this.caption;
    }
    
    public void setCaption(String value) {
        this.caption = value;
    }
    
    public AbstractFlow withCaption(String value) {
        setCaption(value);
        return (AbstractFlow) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.flow.condition.FlowConditionList getConditions() {
        return this.conditions;
    }
    
    public AbstractFlow withCondition(org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition value) {
        getConditions().add(value);
        return (AbstractFlow) this;
    }
    
    public <T extends org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition> T createCondition(Class<T> nodeClass) {
        org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition value = createChildNode(nodeClass, "conditions");
        getConditions().add(value);
        return (T) value;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition createCondition() {
        return createCondition(org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition.class);
    }
    
    public org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode getTargetNode() {
        return this.targetNode;
    }
    
    public void setTargetNode(org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode value) {
        this.targetNode = value;
    }
    
    public AbstractFlow withTargetNode(org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode value) {
        setTargetNode(value);
        return (AbstractFlow) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getConditions()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getTargetNode() != null ) { 
            getTargetNode().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getConditions()) {
            references.add(node);
        }
        
        references.add(getTargetNode());
        
    }
}
