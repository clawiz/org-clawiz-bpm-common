package org.clawiz.bpm.common.metadata.data.process.node.event.start;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class NoneStartEventPrototype extends org.clawiz.bpm.common.metadata.data.process.node.event.start.AbstractStartEvent {
    
    public NoneStartEvent withName(String value) {
        setName(value);
        return (NoneStartEvent) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
