/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service.component.service;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.metadata.data.process.node.activity.AbstractActivity;
import org.clawiz.bpm.common.generator.service.component.AbstractProcessServiceGeneratorJavaClassComponent;
import org.clawiz.bpm.common.generator.service.component.service.element.*;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeIsNodePassedMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeNodeProcessMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.flow.ProcessServicePrototypeIsFlowAllowedMethodElement;
import org.clawiz.bpm.common.manager.service.AbstractProcessService;
import org.clawiz.bpm.common.storage.process.ProcessService;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.metadata.data.type.Type;

import java.math.BigDecimal;
import java.util.HashMap;

public class ProcessServicePrototypeComponent extends AbstractProcessServiceGeneratorJavaClassComponent {


    public static final String AFTER_PROCESS_EXTENSION_NAME = "afterProcess";

    public static String getProcessNameStaticVariableName(Process process) {
        return GeneratorUtils.toJavaName(GeneratorUtils.toJavaNameWithUnderscores(process.getName()).toUpperCase() + "_PROCESS_NAME", true).toUpperCase();
    }

    public static String getActivityNameStaticVariableName(AbstractActivity activity) {
        return GeneratorUtils.toJavaName(GeneratorUtils.toJavaNameWithUnderscores(activity.getName()).toUpperCase() + "_ACTIVITY_NAME", true).toUpperCase();
    }

    public Type getRootType() {
        return getProcess().getRootType();
    }

    protected void addImports() {

        addImport(Process.class);
        addImport(BigDecimal.class);
        addImport(ProcessService.class);
        addImport(getRootType().getServicePackageName() + "." + getRootType().getJavaClassName() + "Object");
        addImport(getRootType().getServicePackageName() + "." + getRootType().getJavaClassName() + "Service");

    }



    public static String getUniqueTypeVariableName(Type type, HashMap<String, String> namesCache) {
        String variableName = namesCache.get(type.getFullName());
        if ( variableName != null ) {
            return variableName;
        }
        variableName = type.getJavaVariableName();
        if ( namesCache.containsKey(variableName) ) {
            variableName = GeneratorUtils.toJavaName(type.getFullName());
        }
        namesCache.put(type.getFullName(), variableName);
        return variableName;
    }

    HashMap<String, String> typeServiceVariablesCache = new HashMap<>();
    public String getRootTypeServiceVariableName() {
        return getUniqueTypeVariableName(getRootType(), typeServiceVariablesCache) + "Service";
    }


    protected void addVariables() {

        addVariable("String", getProcessNameStaticVariableName(getProcess()), "\"" + getProcess().getFullName() + "\"")
                .withAccessLevel(JavaMemberElement.AccessLevel.PUBLIC)
                .withStatic(true)
                .withFinal(true);

        for ( AbstractProcessNode node: getProcess().getNodes() ) {
            if ( ! AbstractActivity.class.isAssignableFrom(node.getClass())) {
                continue;
            }
            AbstractActivity activity = (AbstractActivity) node;
            addVariable("String", getActivityNameStaticVariableName(activity), "\""
                    + activity.getName().replaceAll("\\\"", "\\\\\"")
                    + "\"")
                    .withAccessLevel(JavaMemberElement.AccessLevel.PUBLIC)
                    .withStatic(true)
                    .withFinal(true);
        }

        addVariable("Process", "_process")
                .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE)
                .withStatic(true);

        String typeName = getRootType().getJavaClassName() + "Service";
        addVariable(typeName, getRootTypeServiceVariableName());

    }


    protected void addCommonGetters() {
        addMethod("Process",    "getProcess").addText("return this._process;");
        addMethod("BigDecimal", "getProcessId").addText("return this._process.getId();");


/*
        for ( ProcessRule rule : getProcess().getRules() ) {
            addMethod("ProcessRule", processGeneratorHelper.getRuleServiceGetterName(rule))
                    .addText("return this._process.getRule("+ getActivityNameStaticVariableName(rule) + ");");
        }
*/

    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(ProcessServiceComponent.class).getName() + "Prototype");

        setExtends(AbstractProcessService.class.getName()
                + "<" + getRootType().getJavaClassName()  + "Object, " + getInstanceClassName() + ">");

        addImports();

        addVariables();

        addElement(ProcessServicePrototypeInitMethodElement.class);

        addCommonGetters();

        addElement(ProcessServicePrototypeLoadObjectMethodElement.class);

        for ( AbstractProcessNode node : getProcess().getNodes() ) {

            for ( AbstractFlow flow : node.getOutgoingFlows() ) {
                addElement(ProcessServicePrototypeIsFlowAllowedMethodElement.class).setFlow(flow);
            }

            Class<AbstractProcessServicePrototypeNodeProcessMethodElement> processClass = getGenerator().getNodeProcessMap(node.getClass());
            if ( processClass == null ) {
                throwException("Class ? of node ? has not java class process element map", node.getClass().getName(), node.getFullName() );
            }
            addElement(processClass).setNode(node);

            Class<AbstractProcessServicePrototypeIsNodePassedMethodElement> passedClass = getGenerator().getNodePassedMap(node.getClass());
            if ( passedClass == null ) {
                throwException("Class ? of node ? has not java class isPassed element map", node.getClass().getName(), node.getFullName() );
            }
            addElement(passedClass).setNode(node);

        }

        addElement(ProcessServicePrototypeStartProcessMethodElement.class);

        processExtensions(AFTER_PROCESS_EXTENSION_NAME);

    }
}
