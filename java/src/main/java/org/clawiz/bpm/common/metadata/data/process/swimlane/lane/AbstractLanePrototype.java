package org.clawiz.bpm.common.metadata.data.process.swimlane.lane;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractLanePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractLane withName(String value) {
        setName(value);
        return (AbstractLane) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
