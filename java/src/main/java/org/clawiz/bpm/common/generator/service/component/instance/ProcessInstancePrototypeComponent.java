/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service.component.instance;

import org.clawiz.bpm.common.generator.service.component.AbstractProcessServiceGeneratorJavaClassComponent;
import org.clawiz.bpm.common.generator.service.component.instance.element.ProcessInstancePrototypeFromParameterListMethodElement;
import org.clawiz.bpm.common.generator.service.component.instance.element.ProcessInstancePrototypeToParameterListMethodElement;
import org.clawiz.bpm.common.manager.instance.AbstractProcessInstance;
import org.clawiz.bpm.common.manager.instance.ProcessInstanceParameter;
import org.clawiz.bpm.common.manager.instance.ProcessInstanceParameterList;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ProcessInstancePrototypeComponent extends AbstractProcessServiceGeneratorJavaClassComponent {

    protected void addImports() {
        addImport(BigDecimal.class);
        addImport(ArrayList.class);
        addImport(ProcessInstanceParameter.class);
        addImport(ProcessInstanceParameterList.class);
        addImport(getRootType().getServicePackageName() + "." + getRootTypeObjectClassName());
    }


    protected void addVariables() {

        ArrayList<JavaVariableElement> variables = new ArrayList<>();

        for ( JavaVariableElement ve : variables ) {
            ve.addGetter();
            ve.addSetter();
        }

    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(ProcessInstanceComponent.class).getName() + "Prototype");

        setExtends(AbstractProcessInstance.class.getName() + "<" + getRootTypeObjectClassName() + ">");

        addImports();
        addVariables();

        addElement(ProcessInstancePrototypeToParameterListMethodElement.class);
        addElement(ProcessInstancePrototypeFromParameterListMethodElement.class);

    }
}
