package org.clawiz.bpm.common.generator.service.component;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.generator.service.ProcessGenerator;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.extension.ServiceExtension;
import org.clawiz.core.common.metadata.data.type.Type;

public class AbstractProcessGeneratorJavaClassComponentExtension extends ServiceExtension {

    AbstractProcessServiceGeneratorJavaClassComponent component;

    @Override
    public void setExtendedService(Service extendedService) {
        super.setExtendedService(extendedService);
        component = (AbstractProcessServiceGeneratorJavaClassComponent) extendedService;
    }

    public AbstractProcessServiceGeneratorJavaClassComponent getComponent() {
        return component;
    }

    public ProcessGenerator getGenerator() {
        return component.getGenerator();
    }

    public String getProcessJavaName() {
        return component.getProcessJavaName();
    }

    public String getServiceClassName() {
        return component.getServiceClassName();
    }

    public String getInstanceClassName() {
        return component.getInstanceClassName();
    }

    public Process getProcess() {
        return component.getProcess();
    }

    public Type getRootType() {
        return component.getRootType();
    }

    public String getRootTypeObjectClassName() {
        return component.getRootTypeObjectClassName();
    }

}
