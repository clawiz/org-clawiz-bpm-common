package org.clawiz.bpm.common.metadata.data.process.node.gateway;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractGatewayPrototype extends org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode {
    
    public AbstractGateway withName(String value) {
        setName(value);
        return (AbstractGateway) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
