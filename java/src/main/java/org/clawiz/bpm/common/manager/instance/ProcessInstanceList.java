/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.manager.instance;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ProcessInstanceList implements Iterable<AbstractProcessInstance> {

    ArrayList<AbstractProcessInstance>  instances = new ArrayList<>();

    public int size() {
        return instances.size();
    }

    public void add(AbstractProcessInstance instance) {
        instances.add(instance);
    }

    public AbstractProcessInstance get(int index) {
        return instances.get(index);
    }

    @Override
    public Iterator<AbstractProcessInstance> iterator() {
        return instances.iterator();
    }

    @Override
    public void forEach(Consumer<? super AbstractProcessInstance> action) {
        instances.forEach(action);
    }

    @Override
    public Spliterator<AbstractProcessInstance> spliterator() {
        return instances.spliterator();
    }
}
