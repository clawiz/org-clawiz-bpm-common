package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractFlowPrototype extends AbstractMpsNode {
    
    public String caption;
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition> conditions = new ArrayList<>();
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode targetNode;
    
    public String getCaption() {
        return this.caption;
    }
    
    public void setCaption(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.caption = null;
        }
        this.caption = value;
    }
    
    public ArrayList<org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition> getConditions() {
        return this.conditions;
    }
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode getTargetNode() {
        return this.targetNode;
    }
    
    public void setTargetNode(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode value) {
        this.targetNode = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "645131847789262884";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractFlow";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "645131847789262884", "org.clawiz.bpm.common.language.structure.AbstractFlow", ConceptPropertyType.PROPERTY, "7193934750760394542", "caption"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "645131847789262884", "org.clawiz.bpm.common.language.structure.AbstractFlow", ConceptPropertyType.CHILD, "1512176054398628623", "conditions"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "645131847789262884", "org.clawiz.bpm.common.language.structure.AbstractFlow", ConceptPropertyType.REFERENCE, "7874312983645229931", "targetNode"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("645131847789262884", "caption", getCaption());
        for (AbstractMpsNode value : getConditions() ) {
            addConceptNodeChild("645131847789262884", "conditions", value);
        }
        addConceptNodeRef("645131847789262884", "targetNode", getTargetNode());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow structure = (org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow) node;
        
        structure.setCaption(getCaption());
        
        structure.getConditions().clear();
        for (org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition mpsNode : getConditions() ) {
            if ( mpsNode != null ) {
                structure.getConditions().add((org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition) mpsNode.toMetadataNode(structure, "conditions"));
            } else {
                structure.getConditions().add(null);
            }
        }
        
        if ( getTargetNode() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "targetNode", false, getTargetNode());
        } else {
            structure.setTargetNode(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow structure = (org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow) node;
        
        setCaption(structure.getCaption());
        
        getConditions().clear();
        for (org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition metadataNode : structure.getConditions() ) {
            if ( metadataNode != null ) {
                getConditions().add(loadChildMetadataNode(metadataNode));
            } else {
                getConditions().add(null);
            }
        }
        
        if ( structure.getTargetNode() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "targetNode", false, structure.getTargetNode());
        } else {
            setTargetNode(null);
        }
        
    }
}
