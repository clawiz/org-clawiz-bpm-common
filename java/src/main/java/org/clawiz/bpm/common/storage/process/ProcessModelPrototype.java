package org.clawiz.bpm.common.storage.process;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ProcessModelPrototype extends TypeModel {
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField ROOT_TYPE_ID_FIELD;
    
    private static Type type;
    
    private ProcessService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ProcessService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ProcessService.class).getType();
        
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Process.PackageName"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Process.Name"}); }
        
        ROOT_TYPE_ID_FIELD = type.getFields().get("rootType");
        if ( ROOT_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Process.rootType"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _rootTypeId;
    
    public TypeFieldModel rootTypeId() {
        
        if ( _rootTypeId != null ) {
            return _rootTypeId;
        }
        
        _rootTypeId = new TypeFieldModel(this, ROOT_TYPE_ID_FIELD);
        return _rootTypeId;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PACKAGENAME" : return packageName();
        case "NAME" : return name();
        case "ROOTTYPE" : return rootTypeId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
