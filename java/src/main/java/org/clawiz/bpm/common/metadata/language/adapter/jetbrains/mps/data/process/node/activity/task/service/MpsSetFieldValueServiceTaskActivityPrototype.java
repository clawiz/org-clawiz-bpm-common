package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsSetFieldValueServiceTaskActivityPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service.MpsAbstractServiceTaskActivity {
    
    public String value;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField typeField;
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.value = null;
        }
        this.value = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField getTypeField() {
        return this.typeField;
    }
    
    public void setTypeField(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField value) {
        this.typeField = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1057994329318024008";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.SetFieldValueServiceTaskActivity";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "1057994329318024008", "org.clawiz.bpm.common.language.structure.SetFieldValueServiceTaskActivity", ConceptPropertyType.PROPERTY, "1057994329318024015", "value"));
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "1057994329318024008", "org.clawiz.bpm.common.language.structure.SetFieldValueServiceTaskActivity", ConceptPropertyType.REFERENCE, "1057994329318024012", "typeField"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("1057994329318024008", "value", getValue());
        addConceptNodeRef("1057994329318024008", "typeField", getTypeField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity) node;
        
        structure.setValue(getValue());
        
        if ( getTypeField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "typeField", false, getTypeField());
        } else {
            structure.setTypeField(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity) node;
        
        setValue(structure.getValue());
        
        if ( structure.getTypeField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "typeField", false, structure.getTypeField());
        } else {
            setTypeField(null);
        }
        
    }
}
