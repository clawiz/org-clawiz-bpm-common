/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.manager.instance;

import org.clawiz.core.common.CoreException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ProcessInstanceParameterList implements Iterable<ProcessInstanceParameter> {

    ArrayList<ProcessInstanceParameter>       list       = new ArrayList<>();
    HashMap<String, ProcessInstanceParameter> namesCache = new HashMap<>();

    public void set(ProcessInstanceParameter parameter) {
        if ( parameter == null ) {
            throw new CoreException("Cannot set null process parameter");
        }
        if ( parameter.name == null ) {
            throw new CoreException("Cannot set process parameter with null name");
        }
        if ( namesCache.get(parameter.name) != null ) {
            namesCache.remove(parameter.name);
            for ( int i=0; i < list.size(); i++) {
                if ( list.get(i).name.equals(parameter.name) ) {
                    list.remove(i);
                    break;
                }
            }
        }
        namesCache.put(parameter.name, parameter);
        list.add(parameter);
    }

    public ProcessInstanceParameter get(String name) {
        return name != null ? namesCache.get(name) : null;
    }

    public void setBigDecimal(String name, BigDecimal objectId) {
        ProcessInstanceParameter parameter = new ProcessInstanceParameter();
        parameter.name       = name;
        parameter.bigDecimal = objectId;
        set(parameter);
    }

    public BigDecimal getBigDecimal(String name) {
        ProcessInstanceParameter parameter = get(name);
        return parameter != null ? parameter.bigDecimal : null;
    }

    @Override
    public Iterator<ProcessInstanceParameter> iterator() {
        return list.iterator();
    }

    @Override
    public void forEach(Consumer<? super ProcessInstanceParameter> action) {
        list.forEach(action);
    }

    @Override
    public Spliterator<ProcessInstanceParameter> spliterator() {
        return list.spliterator();
    }
}
