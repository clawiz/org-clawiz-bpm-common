package org.clawiz.bpm.common.generator.service;

import org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeIsNodePassedMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeNodeProcessMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.flow.condition.AbstractProcessServicePrototypeFlowConditionJavaElement;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.extension.ServiceExtension;

public class AbstractProcessGeneratorExtension extends ServiceExtension {

    ProcessGenerator processGenerator;

    @Override
    public void setExtendedService(Service extendedService) {
        super.setExtendedService(extendedService);
        processGenerator = (ProcessGenerator) extendedService;
    }

    public <A extends AbstractProcessNode, E extends AbstractProcessServicePrototypeNodeProcessMethodElement> void addNodeProcessMap(Class<A> nodeClass, Class<E> elementClass) {
        processGenerator.addNodeProcessMap(nodeClass, elementClass);
    }

    public <A extends AbstractProcessNode, E extends AbstractProcessServicePrototypeIsNodePassedMethodElement> void addNodePassedMap(Class<A> nodeClass, Class<E> elementClass) {
        processGenerator.addNodePassedMap(nodeClass, elementClass);
    }

    public <A extends AbstractFlowCondition, E extends AbstractProcessServicePrototypeFlowConditionJavaElement> void addFlowConditionMap(Class<A> conditionClass, Class<E> elemetClass) {
        processGenerator.addFlowConditionMap(conditionClass, elemetClass);
    }


}
