package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsExecuteServiceMethodServiceTaskActivityPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.service.MpsAbstractServiceTaskActivity {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef serviceMethod;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef getServiceMethod() {
        return this.serviceMethod;
    }
    
    public void setServiceMethod(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef value) {
        this.serviceMethod = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "7874312983645323008";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.ExecuteServiceMethodServiceTaskActivity";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "7874312983645323008", "org.clawiz.bpm.common.language.structure.ExecuteServiceMethodServiceTaskActivity", ConceptPropertyType.CHILD, "7874312983645323009", "serviceMethod"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("7874312983645323008", "serviceMethod", getServiceMethod());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.ExecuteServiceMethodServiceTaskActivity.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.ExecuteServiceMethodServiceTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.ExecuteServiceMethodServiceTaskActivity) node;
        
        if ( getServiceMethod().getMethod() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "serviceMethod", false, getServiceMethod().getMethod());
        } else {
            structure.setServiceMethod(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.ExecuteServiceMethodServiceTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.ExecuteServiceMethodServiceTaskActivity) node;
        
        if ( structure.getServiceMethod() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "serviceMethod", false, structure.getServiceMethod());
        } else {
            setServiceMethod(null);
        }
        
    }
}
