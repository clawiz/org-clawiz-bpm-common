package org.clawiz.bpm.common.metadata.data.process.flow.sequence;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractSequenceFlowPrototype extends org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow {
    
    public AbstractSequenceFlow withName(String value) {
        setName(value);
        return (AbstractSequenceFlow) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
