package org.clawiz.bpm.common.metadata.data.process.flow.condition.message;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ReceiveMessageFlowConditionPrototype extends org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition {
    
    @ExchangeReference
    private org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage message;
    
    public ReceiveMessageFlowCondition withName(String value) {
        setName(value);
        return (ReceiveMessageFlowCondition) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage getMessage() {
        return this.message;
    }
    
    public void setMessage(org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage value) {
        this.message = value;
    }
    
    public ReceiveMessageFlowCondition withMessage(org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessage value) {
        setMessage(value);
        return (ReceiveMessageFlowCondition) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getMessage() != null ) { 
            getMessage().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getMessage());
        
    }
}
