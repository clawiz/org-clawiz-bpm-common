package org.clawiz.bpm.common.storage.process;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ProcessServicePrototype extends AbstractTypeService<ProcessObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.bpm.common.storage", "Process");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.Process"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      process Checked object
    */
    public void check(ProcessObject process) {
        
        if ( process == null ) {
            throwException("Cannot check null ?", new Object[]{"ProcessObject"});
        }
        
        process.fillDefaults();
        
        
        if ( packageNameToId(process.getPackageName(), process.getName(), process.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Process", process.toPackageName() });
        }
        
    }
    
    /**
    * Create new ProcessObject instance and fill default values
    * 
    * @return     Created object
    */
    public ProcessObject create() {
        
        ProcessObject process = new ProcessObject();
        process.setService((ProcessService) this);
        
        process.fillDefaults();
        
        return process;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ProcessObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, name, root_type_id from cw_bpm_processes where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Process", id});
        }
        
        ProcessObject result = new ProcessObject();
        
        result.setService((ProcessService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setName(statement.getString(2));
        result.setRootTypeId(statement.getBigDecimal(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ProcessObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ProcessList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ProcessObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ProcessList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ProcessList result = new ProcessList();
        
        
        Statement statement = executeQuery("select id, package_name, name, root_type_id from cw_bpm_processes"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ProcessObject object = new ProcessObject();
        
            object.setService((ProcessService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setName(statement.getString(3));
            object.setRootTypeId(statement.getBigDecimal(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'PackageName' fields
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name) {
        return packageNameToId(packageName, name, null);
    }
    
    /**
    * Find id of record by key 'PackageName' fields with id not equal skipId
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, BigDecimal skipId) {
        
        if ( packageName == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_bpm_processes where upper_package_name = ? and upper_name = ?", packageName.toUpperCase(), name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_bpm_processes where upper_package_name = ? and upper_name = ? and id != ?", packageName.toUpperCase(), name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'PackageName' fields or create new record with values set to given parameters
    * 
    * @param      packageName         PackageName
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = packageNameToId(packageName, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ProcessObject object = create();
        object.setPackageName(packageName);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'PackageName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToPackageName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select package_name, name from cw_bpm_processes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ProcessObject object = new ProcessObject();
        object.setService((ProcessService)this);
        object.setId(id);
        object.setPackageName(statement.getString(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toPackageName();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'RootType' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToRootType(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select root_type_id from cw_bpm_processes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ProcessObject object = new ProcessObject();
        object.setService((ProcessService)this);
        object.setId(id);
        object.setRootTypeId(statement.getBigDecimal(1));
        
        statement.close();
        
        return object.toRootType();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToPackageName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      processObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ProcessObject processObject) {
        return processObject.toPackageName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ProcessObject oldProcessObject, ProcessObject newProcessObject) {
        
        ProcessObject o = oldProcessObject != null ? oldProcessObject : new ProcessObject();
        ProcessObject n = newProcessObject != null ? newProcessObject : new ProcessObject();
        
        
        executeUpdate("insert into a_cw_bpm_processes (scn, action_type, id , o_package_name, o_upper_package_name, o_name, o_upper_name, o_root_type_id, n_package_name, n_upper_package_name, n_name, n_upper_name, n_root_type_id) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getRootTypeId(), n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getRootTypeId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      process Saved object
    */
    public void save(ProcessObject process) {
        
        if ( process == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ProcessObject"});
        }
        
        TransactionAction transactionAction;
        ProcessObject oldProcess;
        if ( process.getService() == null ) {
            process.setService((ProcessService)this);
        }
        
        check(process);
        
        if ( process.getId() == null ) {
        
            process.setId(getObjectService().createObject(getTypeId()));
        
            oldProcess = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, process.getId());
        
            executeUpdate("insert into cw_bpm_processes" 
                          + "( id, package_name, upper_package_name, name, upper_name, root_type_id ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?)",
                          process.getId(), process.getPackageName(), ( process.getPackageName() != null ? process.getPackageName().toUpperCase() : null ), process.getName(), ( process.getName() != null ? process.getName().toUpperCase() : null ), process.getRootTypeId() );
        
            if ( process.getRootTypeId() != null ) { getObjectService().setLink(process.getRootTypeId(), process.getId()); }
        
        } else {
        
            oldProcess = load(process.getId());
            if ( oldProcess.equals(process) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, process.getId());
        
            executeUpdate("update cw_bpm_processes set "
                          + "package_name = ?, upper_package_name = ?, name = ?, upper_name = ?, root_type_id = ? "
                          + "where id = ?",
                          process.getPackageName(), ( process.getPackageName() != null ? process.getPackageName().toUpperCase() : null ), process.getName(), ( process.getName() != null ? process.getName().toUpperCase() : null ), process.getRootTypeId(), process.getId() );
        
            getObjectService().changeLinkParent(oldProcess.getRootTypeId(), process.getRootTypeId(), process.getId());
        
        }
        
        saveAudit(transactionAction, oldProcess, process);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ProcessObject oldProcessObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldProcessObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldProcessObject.getRootTypeId() != null) { getObjectService().deleteLink(oldProcessObject.getRootTypeId(), id); }
        
        executeUpdate("delete from cw_bpm_processes where id = ?", id);
        
    }
}
