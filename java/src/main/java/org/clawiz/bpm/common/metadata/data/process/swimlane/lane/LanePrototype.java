package org.clawiz.bpm.common.metadata.data.process.swimlane.lane;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class LanePrototype extends org.clawiz.bpm.common.metadata.data.process.swimlane.lane.AbstractLane {
    
    public Lane withName(String value) {
        setName(value);
        return (Lane) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
