package org.clawiz.bpm.common.metadata.data.process.node.event.end;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class NoneEndEventPrototype extends org.clawiz.bpm.common.metadata.data.process.node.event.end.AbstractEndEvent {
    
    public NoneEndEvent withName(String value) {
        setName(value);
        return (NoneEndEvent) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
