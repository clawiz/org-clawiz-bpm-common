/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.metadata.data.process.node;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow;
import org.clawiz.bpm.common.metadata.data.process.flow.FlowList;
import org.clawiz.bpm.common.manager.instance.AbstractProcessInstance;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;

import java.lang.reflect.Method;

public class AbstractProcessNode extends AbstractProcessNodePrototype {

    @Override
    public String getParentNodeFieldName() {
        return "nodes";
    }

    public String getDescription() {
        return super.getDescription() != null ? super.getDescription() : getName();
    }

    public Process getProcess() {
        return (Process) getParentNode();
    }

    public boolean isAutoOut() {
        return true;
    }

    private Method isPassedMethod;
    public Method getIsPassedMethod() {

        if ( isPassedMethod == null ) {

            String methodName = "is" + getJavaClassName() + "Passed";
            isPassedMethod = ReflectionUtils.findMethod(getProcess().getServiceClass(), methodName, AbstractProcessInstance.class);

            if ( isPassedMethod == null) {
                throw new CoreException("Method ? for node ? not found in process service class ?", methodName, getFullName(), getProcess().getServiceClass().getName());
            }
        }
        return isPassedMethod;
    }

    private Method processMethod;
    public Method getProcessMethod() {

        if ( processMethod == null ) {

            String methodName = "process" + getJavaClassName();
            processMethod = ReflectionUtils.findMethod(getProcess().getServiceClass(), methodName, AbstractProcessInstance.class);

            if ( processMethod == null) {
                throw new CoreException("Method ? for node ? not found in process service class ?", methodName, getFullName(), getProcess().getServiceClass().getName());
            }
        }
        return processMethod;
    }

    public String getFullJavaVariableName() {
        return getProcess().getJavaVariableName()
                + getJavaClassName();
    }

    public String getFullJavaClassName() {
        return getProcess().getJavaClassName()
                + getJavaClassName();
    }

}
