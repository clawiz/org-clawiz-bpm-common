/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.ui.generator.client.ts.ui.form.component;

import org.clawiz.bpm.common.metadata.data.process.ProcessList;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.system.service.extension.ServiceExtensionMethod;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentPrototypeComponent;

public class BpmTypeScriptFormComponentPrototypeComponentExtension extends AbstractTypeScriptFormClassComponentExtension {


    @ServiceExtensionMethod(name = TypeScriptFormComponentPrototypeComponent.AFTER_PROCESS_EXTENSION_NAME)
    public void afterProcess() {

        if ( form.getRootType() == null ) {
            return;
        }
        ProcessList processes = processGeneratorHelper.getProcessesByRootType(form.getRootType());
        if ( processes.size() == 0 ) {
            return;
        }

        formComponent.addField("processInstances", "any")
                .setInitStatement("{ instances:[] }");


        TypeScriptMethodElement method = formComponent.addMethod("processLoadResponse", "Promise<any>");
        method.addParameter("response", "any");
        method.addText("return super.processLoadResponse(response)");
        method.addText("  .then(() => {");
        method.addText("    this.processInstances = response['processInstances'];");
        method.addText("  });");

        method = formComponent.addMethod("executeFlow");
        method.addParameter("instance", "any");
        method.addParameter("flow", "any");

        method.addText("this.save({");
        method.addText("  closeAfterSave : false");
        method.addText("  ,api               : {");
        method.addText("     action          : 'executeProcessFlow'");
        method.addText("    ,processName     : instance.processName");
        method.addText("    ,processNodeName : flow.nodeName");
        method.addText("    ,processFlowName : flow.name");
        method.addText("  }");
        method.addText("});");

    }

}
