package org.clawiz.bpm.common.metadata.data.process.node;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractProcessNodePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    @ExchangeReference
    private org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane lane;
    
    @ExchangeElement
    private org.clawiz.bpm.common.metadata.data.process.flow.FlowList outgoingFlows = new org.clawiz.bpm.common.metadata.data.process.flow.FlowList();
    
    public AbstractProcessNode withName(String value) {
        setName(value);
        return (AbstractProcessNode) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public AbstractProcessNode withDescription(String value) {
        setDescription(value);
        return (AbstractProcessNode) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane getLane() {
        return this.lane;
    }
    
    public void setLane(org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane value) {
        this.lane = value;
    }
    
    public AbstractProcessNode withLane(org.clawiz.bpm.common.metadata.data.process.swimlane.lane.Lane value) {
        setLane(value);
        return (AbstractProcessNode) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.flow.FlowList getOutgoingFlows() {
        return this.outgoingFlows;
    }
    
    public AbstractProcessNode withOutgoingFlow(org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow value) {
        getOutgoingFlows().add(value);
        return (AbstractProcessNode) this;
    }
    
    public <T extends org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow> T createOutgoingFlow(Class<T> nodeClass) {
        org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow value = createChildNode(nodeClass, "outgoingFlows");
        getOutgoingFlows().add(value);
        return (T) value;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow createOutgoingFlow() {
        return createOutgoingFlow(org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getLane() != null ) { 
            getLane().prepare(session);
        }
        for (MetadataNode node : getOutgoingFlows()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getLane());
        
        for (MetadataNode node : getOutgoingFlows()) {
            references.add(node);
        }
        
    }
}
