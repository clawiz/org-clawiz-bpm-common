package org.clawiz.bpm.common.metadata.data.process.flow.condition.eval;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractEvalFlowConditionPrototype extends org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition {
    
    @ExchangeAttribute
    private String expression;
    
    public AbstractEvalFlowCondition withName(String value) {
        setName(value);
        return (AbstractEvalFlowCondition) this;
    }
    
    public String getExpression() {
        return this.expression;
    }
    
    public void setExpression(String value) {
        this.expression = value;
    }
    
    public AbstractEvalFlowCondition withExpression(String value) {
        setExpression(value);
        return (AbstractEvalFlowCondition) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
