package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.send;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractSendTaskActivityPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.activity.task.message.MpsAbstractMessageTaskActivity {
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.MpsProcess targetProcess;
    
    public org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.MpsProcess getTargetProcess() {
        return this.targetProcess;
    }
    
    public void setTargetProcess(org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.MpsProcess value) {
        this.targetProcess = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "1512176054398374489";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractSendTaskActivity";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "1512176054398374489", "org.clawiz.bpm.common.language.structure.AbstractSendTaskActivity", ConceptPropertyType.REFERENCE, "1512176054398374633", "targetProcess"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("1512176054398374489", "targetProcess", getTargetProcess());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send.AbstractSendTaskActivity.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send.AbstractSendTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send.AbstractSendTaskActivity) node;
        
        if ( getTargetProcess() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "targetProcess", false, getTargetProcess());
        } else {
            structure.setTargetProcess(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getTargetProcess());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send.AbstractSendTaskActivity structure = (org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send.AbstractSendTaskActivity) node;
        
        if ( structure.getTargetProcess() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "targetProcess", false, structure.getTargetProcess());
        } else {
            setTargetProcess(null);
        }
        
    }
}
