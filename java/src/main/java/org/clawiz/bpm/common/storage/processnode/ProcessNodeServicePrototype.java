package org.clawiz.bpm.common.storage.processnode;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ProcessNodeServicePrototype extends AbstractTypeService<ProcessNodeObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.bpm.common.storage", "ProcessNode");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.bpm.common.storage.ProcessNode"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      processNode Checked object
    */
    public void check(ProcessNodeObject processNode) {
        
        if ( processNode == null ) {
            throwException("Cannot check null ?", new Object[]{"ProcessNodeObject"});
        }
        
        processNode.fillDefaults();
        
        
        if ( processNameToId(processNode.getProcessId(), processNode.getName(), processNode.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "ProcessNode", processNode.toProcessName() });
        }
        
    }
    
    /**
    * Create new ProcessNodeObject instance and fill default values
    * 
    * @return     Created object
    */
    public ProcessNodeObject create() {
        
        ProcessNodeObject processNode = new ProcessNodeObject();
        processNode.setService((ProcessNodeService) this);
        
        processNode.fillDefaults();
        
        return processNode;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ProcessNodeObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select process_id, name from cw_bpm_process_nodes where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"ProcessNode", id});
        }
        
        ProcessNodeObject result = new ProcessNodeObject();
        
        result.setService((ProcessNodeService) this);
        result.setId(id);
        result.setProcessId(statement.getBigDecimal(1));
        result.setName(statement.getString(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ProcessNodeObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ProcessNodeList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ProcessNodeObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ProcessNodeList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ProcessNodeList result = new ProcessNodeList();
        
        
        Statement statement = executeQuery("select id, process_id, name from cw_bpm_process_nodes"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ProcessNodeObject object = new ProcessNodeObject();
        
            object.setService((ProcessNodeService) this);
            object.setId(statement.getBigDecimal(1));
            object.setProcessId(statement.getBigDecimal(2));
            object.setName(statement.getString(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'ProcessName' fields
    * 
    * @param      processId Process
    * @param      name      Name
    * @return     Id of found record or null
    */
    public BigDecimal processNameToId(java.math.BigDecimal processId, java.lang.String name) {
        return processNameToId(processId, name, null);
    }
    
    /**
    * Find id of record by key 'ProcessName' fields with id not equal skipId
    * 
    * @param      processId Process
    * @param      name      Name
    * @param      skipId    Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal processNameToId(java.math.BigDecimal processId, java.lang.String name, BigDecimal skipId) {
        
        if ( processId == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_bpm_process_nodes where process_id = ? and upper_name = ?", processId, name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_bpm_process_nodes where process_id = ? and upper_name = ? and id != ?", processId, name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'ProcessName' fields or create new record with values set to given parameters
    * 
    * @param      processId           Process
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal processNameToId(java.math.BigDecimal processId, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = processNameToId(processId, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ProcessNodeObject object = create();
        object.setProcessId(processId);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ProcessName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToProcessName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select process_id, name from cw_bpm_process_nodes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ProcessNodeObject object = new ProcessNodeObject();
        object.setService((ProcessNodeService)this);
        object.setId(id);
        object.setProcessId(statement.getBigDecimal(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toProcessName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToProcessName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      processNodeObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ProcessNodeObject processNodeObject) {
        return processNodeObject.toProcessName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ProcessNodeObject oldProcessNodeObject, ProcessNodeObject newProcessNodeObject) {
        
        ProcessNodeObject o = oldProcessNodeObject != null ? oldProcessNodeObject : new ProcessNodeObject();
        ProcessNodeObject n = newProcessNodeObject != null ? newProcessNodeObject : new ProcessNodeObject();
        
        
        executeUpdate("insert into a_cw_bpm_process_nodes (scn, action_type, id , o_process_id, o_name, o_upper_name, n_process_id, n_name, n_upper_name) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getProcessId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, n.getProcessId(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      processNode Saved object
    */
    public void save(ProcessNodeObject processNode) {
        
        if ( processNode == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ProcessNodeObject"});
        }
        
        TransactionAction transactionAction;
        ProcessNodeObject oldProcessNode;
        if ( processNode.getService() == null ) {
            processNode.setService((ProcessNodeService)this);
        }
        
        check(processNode);
        
        if ( processNode.getId() == null ) {
        
            processNode.setId(getObjectService().createObject(getTypeId()));
        
            oldProcessNode = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, processNode.getId());
        
            executeUpdate("insert into cw_bpm_process_nodes" 
                          + "( id, process_id, name, upper_name ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          processNode.getId(), processNode.getProcessId(), processNode.getName(), ( processNode.getName() != null ? processNode.getName().toUpperCase() : null ) );
        
            if ( processNode.getProcessId() != null ) { getObjectService().setLink(processNode.getProcessId(), processNode.getId()); }
        
        } else {
        
            oldProcessNode = load(processNode.getId());
            if ( oldProcessNode.equals(processNode) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, processNode.getId());
        
            executeUpdate("update cw_bpm_process_nodes set "
                          + "process_id = ?, name = ?, upper_name = ? "
                          + "where id = ?",
                          processNode.getProcessId(), processNode.getName(), ( processNode.getName() != null ? processNode.getName().toUpperCase() : null ), processNode.getId() );
        
            getObjectService().changeLinkParent(oldProcessNode.getProcessId(), processNode.getProcessId(), processNode.getId());
        
        }
        
        saveAudit(transactionAction, oldProcessNode, processNode);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ProcessNodeObject oldProcessNodeObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldProcessNodeObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldProcessNodeObject.getProcessId() != null) { getObjectService().deleteLink(oldProcessNodeObject.getProcessId(), id); }
        
        executeUpdate("delete from cw_bpm_process_nodes where id = ?", id);
        
    }
}
