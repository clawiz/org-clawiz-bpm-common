package org.clawiz.bpm.common.metadata.data.process.node.activity.task.message;

import org.clawiz.bpm.common.install.MessageInstaller;
import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;

public class AbstractMessage extends AbstractMessagePrototype {

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) MessageInstaller.class;
    }
}
