package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.service;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsServiceMethodFlowConditionPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.MpsAbstractFlowCondition {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef serviceMethod;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef getServiceMethod() {
        return this.serviceMethod;
    }
    
    public void setServiceMethod(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef value) {
        this.serviceMethod = value;
    }
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "5808768823966602763";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.ServiceMethodFlowCondition";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("e6481df8-cfc6-4d97-a970-182a6ace693b", "org.clawiz.bpm.common.language", "5808768823966602763", "org.clawiz.bpm.common.language.structure.ServiceMethodFlowCondition", ConceptPropertyType.CHILD, "5808768823966602764", "serviceMethod"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeChild("5808768823966602763", "serviceMethod", getServiceMethod());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition structure = (org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition) node;
        
        if ( getServiceMethod().getMethod() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "serviceMethod", false, getServiceMethod().getMethod());
        } else {
            structure.setServiceMethod(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition structure = (org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition) node;
        
        if ( structure.getServiceMethod() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "serviceMethod", false, structure.getServiceMethod());
        } else {
            setServiceMethod(null);
        }
        
    }
}
