package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.event;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractEventPrototype extends org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.node.MpsAbstractProcessNode {
    
    public String getLanguageId() {
        return "e6481df8-cfc6-4d97-a970-182a6ace693b";
    }
    
    public String getLanguageName() {
        return "org.clawiz.bpm.common.language";
    }
    
    public String getLanguageConceptId() {
        return "645131847789189716";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.bpm.common.language.structure.AbstractEvent";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("1169194658468", "name", getName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.bpm.common.metadata.data.process.node.event.AbstractEvent.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.event.AbstractEvent structure = (org.clawiz.bpm.common.metadata.data.process.node.event.AbstractEvent) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.bpm.common.metadata.data.process.node.event.AbstractEvent structure = (org.clawiz.bpm.common.metadata.data.process.node.event.AbstractEvent) node;
        
    }
}
