package org.clawiz.bpm.common.metadata.language.adapter.jetbrains.mps.data.process.flow.condition.service;


import org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethod;
import org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.method.MpsServiceMethodRef;
import org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.service.MpsServiceNode;

public class MpsServiceMethodFlowCondition extends MpsServiceMethodFlowConditionPrototype {

    public void setServiceMethod(MpsServiceMethod serviceMethod) {
        MpsServiceMethodRef ref = createChildNode(MpsServiceMethodRef.class);
        ref.setService((MpsServiceNode) serviceMethod.getParent());
        ref.setMethod(serviceMethod);
        setServiceMethod(ref);
    }

}
