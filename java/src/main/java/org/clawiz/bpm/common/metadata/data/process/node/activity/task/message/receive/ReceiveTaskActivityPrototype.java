package org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.receive;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ReceiveTaskActivityPrototype extends org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.receive.AbstractReceiveTaskActivity {
    
    public ReceiveTaskActivity withName(String value) {
        setName(value);
        return (ReceiveTaskActivity) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
