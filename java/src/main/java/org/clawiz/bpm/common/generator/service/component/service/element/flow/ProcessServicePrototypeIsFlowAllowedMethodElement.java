/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service.component.service.element.flow;

import org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow;
import org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition;
import org.clawiz.bpm.common.generator.service.component.service.element.AbstractProcessServicePrototypeJavaMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.flow.condition.AbstractProcessServicePrototypeFlowConditionJavaElement;

public class ProcessServicePrototypeIsFlowAllowedMethodElement extends AbstractProcessServicePrototypeJavaMethodElement {

    AbstractFlow flow;

    public AbstractFlow getFlow() {
        return flow;
    }

    public void setFlow(AbstractFlow flow) {
        this.flow = flow;
    }

    @Override
    public void process() {
        super.process();

        setName("is" + getFlow().getSourceNode().getJavaClassName() + getFlow().getJavaClassName() + "Allowed");
        setType("boolean");

        addParameter(getInstanceClassName(), "instance");

//        addText("boolean result = true;");

        for (AbstractFlowCondition condition : flow.getConditions() ) {
            Class<AbstractProcessServicePrototypeFlowConditionJavaElement> clazz = getGenerator().getFlowConditionMap(condition.getClass());
            if ( clazz == null ) {
                throwException("Condition class ? of flow ? has not java class element map", condition.getClass().getName(), getFlow().getFullName() );
            }
            AbstractProcessServicePrototypeFlowConditionJavaElement element = addElement(clazz);
            element.setLpad("    ");
            element.setCondition(condition);

        }


        addText("return true;");

    }
}
