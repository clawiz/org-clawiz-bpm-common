/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.ui.helper;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.flow.AbstractFlow;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.helper.ProcessHelper;
import org.clawiz.bpm.common.manager.ProcessManager;
import org.clawiz.bpm.common.manager.instance.AbstractProcessInstance;
import org.clawiz.bpm.common.metadata.data.process.node.event.start.AbstractStartEvent;
import org.clawiz.bpm.common.metadata.data.process.node.event.start.NoneStartEvent;
import org.clawiz.bpm.common.ui.helper.response.ProcessInstancesResponseContext;
import org.clawiz.bpm.common.ui.helper.response.ProcessInstanceResponse;
import org.clawiz.bpm.common.ui.helper.response.ProcessInstanceResponseFlow;
import org.clawiz.bpm.common.ui.helper.response.ProcessInstanceResponseNode;
import org.clawiz.core.common.system.service.Service;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class UiProcessHelper extends Service {

    ProcessManager processManager;
    ProcessHelper  processHelper;

    private ProcessInstancesResponseContext getObjectInstancesResponseContext(BigDecimal objectId) {

        ProcessInstancesResponseContext response = new ProcessInstancesResponseContext();

        for (AbstractProcessInstance instance : processManager.getObjectInstanceList(objectId) ) {
            ProcessInstanceResponse responseInstance = new ProcessInstanceResponse(instance.getProcess().getFullName());
            response.getInstances().add(responseInstance);

            for (AbstractProcessNode node : instance.getNodes() ) {
                responseInstance.getNodes().add(new ProcessInstanceResponseNode(node.getName(), node.getDescription()));
            }

            for ( AbstractFlow flow : processManager.getAllowedFlows(instance) ) {
                responseInstance.getAllowedFlows().add(new ProcessInstanceResponseFlow(flow.getSourceNode().getName(), flow.getName(), flow.getCaption()));
            }

        }

        return response;
    }

    private static ConcurrentHashMap<BigDecimal, ProcessInstancesResponseContext> newObjectsResponseCache = new ConcurrentHashMap<>();

    private ProcessInstancesResponseContext getNewObjectResponse(BigDecimal typeId) {

        ProcessInstancesResponseContext response = new ProcessInstancesResponseContext();

        for (Process process : processHelper.getProcessesByRootType(typeId) ) {
            if ( process.isStartOnNew() == null || ! process.isStartOnNew() ) {
                continue;
            }
            ProcessInstanceResponse responseInstance = new ProcessInstanceResponse(process.getFullName());
            response.getInstances().add(responseInstance);


            for ( AbstractProcessNode newNode : process.getStartNodes() ) {

                if ( ! NoneStartEvent.class.isAssignableFrom(newNode.getClass()) ) {
                    continue;
                }

                for (AbstractFlow afterNewFlow : newNode.getOutgoingFlows() ) {

                    AbstractProcessNode node = afterNewFlow.getTargetNode();

                    if ( node.isAutoOut() ) {
                        continue;
                    }

                    responseInstance.getNodes().add(new ProcessInstanceResponseNode(node.getName(), node.getDescription()));

                    for (AbstractFlow flow : node.getOutgoingFlows() ) {
                        responseInstance.getAllowedFlows().add(new ProcessInstanceResponseFlow(node.getName(), flow.getName(), flow.getCaption()));
                    }

                }


            }

        }

        return response;
    }

    private ProcessInstancesResponseContext getTypeInstancesResponseContext(BigDecimal typeId) {

        ProcessInstancesResponseContext cached = newObjectsResponseCache.get(typeId);
        if ( cached == null ) {
            cached = getNewObjectResponse(typeId);
            newObjectsResponseCache.put(typeId, cached);
        }

        ProcessInstancesResponseContext response = new ProcessInstancesResponseContext();

        for (ProcessInstanceResponse cachedInstance : cached.getInstances() ) {
            ProcessInstanceResponse responseInstance = new ProcessInstanceResponse(cachedInstance.getProcessName());
            response.getInstances().add(responseInstance);
            responseInstance.getNodes().addAll(cachedInstance.getNodes());

            for (ProcessInstanceResponseFlow cachedRule : cachedInstance.getAllowedFlows() ) {
                responseInstance.getAllowedFlows().add(cachedRule);
            }

        }

        return response;
    }

    public ProcessInstancesResponseContext getInstancesResponseContext(BigDecimal objectId, BigDecimal typeId) {
        return objectId != null ? getObjectInstancesResponseContext(objectId) : getTypeInstancesResponseContext(typeId);
    }

}
