package org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractSendTaskActivityPrototype extends org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.AbstractMessageTaskActivity {
    
    @ExchangeReference
    private org.clawiz.bpm.common.metadata.data.process.Process targetProcess;
    
    public AbstractSendTaskActivity withName(String value) {
        setName(value);
        return (AbstractSendTaskActivity) this;
    }
    
    public org.clawiz.bpm.common.metadata.data.process.Process getTargetProcess() {
        return this.targetProcess;
    }
    
    public void setTargetProcess(org.clawiz.bpm.common.metadata.data.process.Process value) {
        this.targetProcess = value;
    }
    
    public AbstractSendTaskActivity withTargetProcess(org.clawiz.bpm.common.metadata.data.process.Process value) {
        setTargetProcess(value);
        return (AbstractSendTaskActivity) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getTargetProcess() != null ) { 
            getTargetProcess().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getTargetProcess());
        
    }
}
