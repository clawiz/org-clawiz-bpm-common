/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.bpm.common.generator.service;

import org.clawiz.bpm.common.metadata.data.process.Process;
import org.clawiz.bpm.common.metadata.data.process.flow.condition.AbstractFlowCondition;
import org.clawiz.bpm.common.metadata.data.process.flow.condition.eval.JavaScriptEvalFlowCondition;
import org.clawiz.bpm.common.metadata.data.process.flow.condition.message.ReceiveMessageFlowCondition;
import org.clawiz.bpm.common.metadata.data.process.flow.condition.service.ServiceMethodFlowCondition;
import org.clawiz.bpm.common.metadata.data.process.node.AbstractProcessNode;
import org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.receive.ReceiveTaskActivity;
import org.clawiz.bpm.common.metadata.data.process.node.activity.task.message.send.SendTaskActivity;
import org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.ExecuteServiceMethodServiceTaskActivity;
import org.clawiz.bpm.common.metadata.data.process.node.activity.task.service.SetFieldValueServiceTaskActivity;
import org.clawiz.bpm.common.metadata.data.process.node.activity.task.user.UserTaskActivity;
import org.clawiz.bpm.common.metadata.data.process.node.event.end.NoneEndEvent;
import org.clawiz.bpm.common.metadata.data.process.node.event.start.NoneStartEvent;
import org.clawiz.bpm.common.generator.service.component.instance.ProcessInstanceComponent;
import org.clawiz.bpm.common.generator.service.component.instance.ProcessInstancePrototypeComponent;
import org.clawiz.bpm.common.generator.service.component.service.ProcessServiceComponent;
import org.clawiz.bpm.common.generator.service.component.service.ProcessServicePrototypeComponent;
import org.clawiz.bpm.common.generator.service.component.service.element.flow.condition.ProcessServicePrototypeEmptyFlowConditionJavaElement;
import org.clawiz.bpm.common.generator.service.component.service.element.flow.condition.ProcessServiceServiceMethodFlowConditionJavaElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeIsNodePassedMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.AbstractProcessServicePrototypeNodeProcessMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.ProcessServicePrototypeEmptyIsNodePassedMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.ProcessServicePrototypeEmptyNodeProcessMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.activity.message.receive.ProcessServicePrototypeReceiveTaskActivityProcessMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.activity.message.send.ProcessServicePrototypeSendTaskActivityProcessMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.activity.service.ProcessServicePrototypeExecuteServiceMethodServiceTaskActivityMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.activity.service.ProcessServicePrototypeSetFieldValueServiceTaskActivityMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.node.activity.user.ProcessServicePrototypeUserTaskActivityMethodElement;
import org.clawiz.bpm.common.generator.service.component.service.element.flow.condition.AbstractProcessServicePrototypeFlowConditionJavaElement;
import org.clawiz.core.common.system.generator.java.AbstractJavaClassGenerator;

import java.util.HashMap;

public class ProcessGenerator extends AbstractJavaClassGenerator {

    public static final String AFTER_PREPARE_EXTENSION_NAME = "afterPrepare";

    Process process;

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
        setPackageName(process.getPackageName() + "." + process.getJavaName().toLowerCase());
    }

    HashMap<Class, Class> nodeProcessMap = new HashMap<>();
    public <A extends AbstractProcessNode, E extends AbstractProcessServicePrototypeNodeProcessMethodElement> void addNodeProcessMap(Class<A> nodeClass, Class<E> elementClass) {
        nodeProcessMap.put(nodeClass, elementClass);
    }

    public <A extends AbstractProcessNode, E extends AbstractProcessServicePrototypeNodeProcessMethodElement> Class<E> getNodeProcessMap(Class<A> activityClass) {
        return nodeProcessMap.get(activityClass);
    }

    HashMap<Class, Class> nodePassedMap = new HashMap<>();
    public <A extends AbstractProcessNode, E extends AbstractProcessServicePrototypeIsNodePassedMethodElement> void addNodePassedMap(Class<A> nodeClass, Class<E> elementClass) {
        nodePassedMap.put(nodeClass, elementClass);
    }

    public <A extends AbstractProcessNode, E extends AbstractProcessServicePrototypeIsNodePassedMethodElement> Class<E> getNodePassedMap(Class<A> activityClass) {
        return nodePassedMap.get(activityClass);
    }

    HashMap<Class, Class> conditionsMap = new HashMap<>();
    public <A extends AbstractFlowCondition, E extends AbstractProcessServicePrototypeFlowConditionJavaElement> void addFlowConditionMap(Class<A> conditionClass, Class<E> elemetClass) {
        conditionsMap.put(conditionClass, elemetClass);
    }

    public <A extends AbstractFlowCondition, E extends AbstractProcessServicePrototypeFlowConditionJavaElement> Class<E> getFlowConditionMap(Class<A> conditionClass) {
        return conditionsMap.get(conditionClass);
    }

    @Override
    protected void prepare() {
        super.prepare();

        addNodePassedMap( UserTaskActivity.class,                        ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(UserTaskActivity.class,                        ProcessServicePrototypeUserTaskActivityMethodElement.class);

        addNodePassedMap( ExecuteServiceMethodServiceTaskActivity.class, ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(ExecuteServiceMethodServiceTaskActivity.class, ProcessServicePrototypeExecuteServiceMethodServiceTaskActivityMethodElement.class);

        addNodePassedMap( SetFieldValueServiceTaskActivity.class, ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(SetFieldValueServiceTaskActivity.class, ProcessServicePrototypeSetFieldValueServiceTaskActivityMethodElement.class);

        addNodePassedMap( SendTaskActivity.class, ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(SendTaskActivity.class, ProcessServicePrototypeSendTaskActivityProcessMethodElement.class);

        addNodePassedMap( ReceiveTaskActivity.class, ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(ReceiveTaskActivity.class, ProcessServicePrototypeReceiveTaskActivityProcessMethodElement.class);

        addNodePassedMap( NoneStartEvent.class, ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(NoneStartEvent.class, ProcessServicePrototypeEmptyNodeProcessMethodElement.class);

        addNodePassedMap( NoneEndEvent.class, ProcessServicePrototypeEmptyIsNodePassedMethodElement.class);
        addNodeProcessMap(NoneEndEvent.class, ProcessServicePrototypeEmptyNodeProcessMethodElement.class);


        addFlowConditionMap(ReceiveMessageFlowCondition.class, ProcessServicePrototypeEmptyFlowConditionJavaElement.class);
        addFlowConditionMap(JavaScriptEvalFlowCondition.class, ProcessServicePrototypeEmptyFlowConditionJavaElement.class);
        addFlowConditionMap(ServiceMethodFlowCondition.class,  ProcessServiceServiceMethodFlowConditionJavaElement.class);


        processExtensions(AFTER_PREPARE_EXTENSION_NAME);

    }

    @Override
    protected void process() {

        logInfo("Start generating process service " + getProcess().getFullName());

        super.process();

        addComponent(ProcessInstanceComponent.class);
        addComponent(ProcessInstancePrototypeComponent.class);

        addComponent(ProcessServiceComponent.class);
        addComponent(ProcessServicePrototypeComponent.class);

    }

    @Override
    protected void done() {
        super.done();

        logInfo("Done generating process service " + getProcess().getFullName());

    }


}
